-- MySQL dump 10.13  Distrib 5.7.10, for Linux (x86_64)
--
-- Host: localhost    Database: ativawealth
-- ------------------------------------------------------
-- Server version	5.7.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ativa_wealth`
--

DROP TABLE IF EXISTS `ativa_wealth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ativa_wealth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ativa_wealth`
--

LOCK TABLES `ativa_wealth` WRITE;
/*!40000 ALTER TABLE `ativa_wealth` DISABLE KEYS */;
INSERT INTO `ativa_wealth` VALUES (1,'<p>Trabalhamos de forma independente com produtos exclusivos e de acordo com o perfil do investidor.</p>\r\n\r\n<p>Atrav&eacute;s de uma an&aacute;lise individualizada de cada um de nossos clientes, buscamos o melhor perfil de investimento visando a melhor rela&ccedil;&atilde;o entre risco e retorno e a preserva&ccedil;&atilde;o de capital ao longo dos anos.</p>\r\n\r\n<p>Levando em considera&ccedil;&atilde;o seus objetivos e necessidades, a nossa gest&atilde;o de patrim&ocirc;nio atua focada em voc&ecirc;.</p>\r\n\r\n<p>S&atilde;o solu&ccedil;&otilde;es e gest&atilde;o para os seus investimentos, com assessoria exclusiva para sua empresa e um &oacute;timo planejamento para o seu futuro.</p>\r\n\r\n<p>Oferecemos um conjunto completo de compet&ecirc;ncias para administrar os seus investimentos de forma personalizada.</p>\r\n\r\n<p>Com produtos diversificados e uma gest&atilde;o eficiente, seu patrim&ocirc;nio ser&aacute; preservado e voc&ecirc; poder&aacute; alcan&ccedil;ar ganhos cada vez maiores.</p>\r\n\r\n<p>Temos uma s&oacute;lida experi&ecirc;ncia na gest&atilde;o e no aconselhamento em diferentes mercados.</p>\r\n','youtube','BsogwgZJW_Q','bg5_20160225151326.jpg','0000-00-00 00:00:00','2016-05-31 16:54:41');
/*!40000 ALTER TABLE `ativa_wealth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` text COLLATE utf8_unicode_ci,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,0,'1_20160531170029.jpg','Lorem ipsum dolor sit','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus quae repudiandae, quis delectus optio reprehenderit, voluptas ipsa! Quaerat corporis saepe quasi ex, ipsum vero, facilis odit inventore earum quia quae.','#','2016-05-31 17:00:30','2016-05-31 17:00:30'),(2,0,'2_20160531170104.jpg','Exemplo','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga nesciunt commodi vel laborum reprehenderit quisquam.','#','2016-05-31 17:01:05','2016-05-31 17:01:05');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carta_ativa`
--

DROP TABLE IF EXISTS `carta_ativa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carta_ativa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carta_ativa`
--

LOCK TABLES `carta_ativa` WRITE;
/*!40000 ALTER TABLE `carta_ativa` DISABLE KEYS */;
INSERT INTO `carta_ativa` VALUES (1,'2016-05','Exemplo','20160531165829_exemplo.txt','2016-05-31 16:58:29','2016-05-31 16:58:29');
/*!40000 ALTER TABLE `carta_ativa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carta_ativa_bg`
--

DROP TABLE IF EXISTS `carta_ativa_bg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carta_ativa_bg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carta_ativa_bg`
--

LOCK TABLES `carta_ativa_bg` WRITE;
/*!40000 ALTER TABLE `carta_ativa_bg` DISABLE KEYS */;
INSERT INTO `carta_ativa_bg` VALUES (1,'bg1_20160225150832.jpg','0000-00-00 00:00:00','2016-05-31 16:57:42');
/*!40000 ALTER TABLE `carta_ativa_bg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `saopaulo` text COLLATE utf8_unicode_ci,
  `riodejaneiro` text COLLATE utf8_unicode_ci,
  `curitiba` text COLLATE utf8_unicode_ci,
  `portoalegre` text COLLATE utf8_unicode_ci,
  `belohorizonte` text COLLATE utf8_unicode_ci,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'bg4_20160225152031.jpg','<p>Rua Iguatemi, 192<br />\r\n15&ordm; e 16&ordm; andares<br />\r\nItaim Bibi<br />\r\nS&atilde;o Paulo/SP<br />\r\n01451-011</p>\r\n\r\n<p>11 4950-7200<br />\r\n11 4097-7200</p>\r\n\r\n<p>Ouvidoria:<br />\r\n11 1234-5678</p>\r\n\r\n<p><a href=\"mailto:ativawm@ativawm.com.br\">ativawm@ativawm.com.br</a></p>\r\n','<p>Av. das Am&eacute;ricas, 3500<br />\r\nSls 314 318 - Le Monde<br />\r\nEd. Londres<br />\r\nBarra da Tijuca<br />\r\nRio de Janeiro/RJ<br />\r\n22640-102</p>\r\n\r\n<p>21 3515-0200<br />\r\n21 3958-0200</p>\r\n','<p>Al. Dr. Carlos de Carvalho, 417<br />\r\n30&ordm; andar, Sala 300<br />\r\nCentro<br />\r\nCuritiba/PR<br />\r\n80410-180</p>\r\n\r\n<p>41 3075-7400</p>\r\n','<p>Av. Nilo Pe&ccedil;anha, 2825<br />\r\nEd. Iguatemi Corporate<br />\r\nConjunto 1503<br />\r\nTr&ecirc;s Figueiras<br />\r\nPorto Alegre/RS<br />\r\n91330-001</p>\r\n\r\n<p>51 3017-8707</p>\r\n','<p>Rua dos Amor&eacute;s, 2001<br />\r\nSls 405 / 406<br />\r\nLourdes<br />\r\nBelo Horizonte/MG<br />\r\n30140-072</p>\r\n\r\n<p>31 3025-0601</p>\r\n','https://www.facebook.com/AtivaWM','https://twitter.com/ativawm','contato@ativawealth.com.br','0000-00-00 00:00:00','2016-05-31 16:51:16');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mensagem` text COLLATE utf8_unicode_ci,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentos`
--

DROP TABLE IF EXISTS `documentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `documentos_categoria_id` int(10) unsigned DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `documentos_documentos_categoria_id_foreign` (`documentos_categoria_id`),
  CONSTRAINT `documentos_documentos_categoria_id_foreign` FOREIGN KEY (`documentos_categoria_id`) REFERENCES `documentos_categorias` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentos`
--

LOCK TABLES `documentos` WRITE;
/*!40000 ALTER TABLE `documentos` DISABLE KEYS */;
INSERT INTO `documentos` VALUES (1,1,0,'Código de Conduta e Ética e Anticorrupção 072015','codigo-conduta-etica-anticorrupcao_20160531171152.pdf','2016-05-31 17:11:52','2016-05-31 17:11:52'),(2,1,1,'Política de Gerenciamento de Risco 2015','politica-de-gerenciamento-de-risco-2015_20160531171224.pdf','2016-05-31 17:12:24','2016-05-31 17:12:24'),(3,1,2,'ANEXO II Fomulário de referência','anexoii-fomulario-de-referencia_20160531171239.pdf','2016-05-31 17:12:39','2016-05-31 17:12:39');
/*!40000 ALTER TABLE `documentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentos_categorias`
--

DROP TABLE IF EXISTS `documentos_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentos_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentos_categorias`
--

LOCK TABLES `documentos_categorias` WRITE;
/*!40000 ALTER TABLE `documentos_categorias` DISABLE KEYS */;
INSERT INTO `documentos_categorias` VALUES (1,0,'MANUAIS E POLÍTICAS','2016-05-31 17:10:44','2016-05-31 17:10:44');
/*!40000 ALTER TABLE `documentos_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filosofia`
--

DROP TABLE IF EXISTS `filosofia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filosofia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `background_abertura` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto_abertura_1` text COLLATE utf8_unicode_ci,
  `texto_abertura_2` text COLLATE utf8_unicode_ci,
  `background_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_1` text COLLATE utf8_unicode_ci,
  `texto_1` text COLLATE utf8_unicode_ci,
  `background_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_2` text COLLATE utf8_unicode_ci,
  `texto_2` text COLLATE utf8_unicode_ci,
  `background_3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_3` text COLLATE utf8_unicode_ci,
  `texto_3` text COLLATE utf8_unicode_ci,
  `background_4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_4` text COLLATE utf8_unicode_ci,
  `texto_4` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filosofia`
--

LOCK TABLES `filosofia` WRITE;
/*!40000 ALTER TABLE `filosofia` DISABLE KEYS */;
INSERT INTO `filosofia` VALUES (1,'bg8_20160225152828.jpg','<p>Contribuir para a rentabiliza&ccedil;&atilde;o do patrim&ocirc;nio de nossos clientes, por meio de orienta&ccedil;&otilde;es t&eacute;cnicas adequadas e perceitos &eacute;ticos, garantindo relacionamentos fi&eacute;is e duradouros.</p>\r\n','<p>Nossas parcerias v&atilde;o mais longe porque vemos de perto as necessidades dos cliente.</p>\r\n','bg5_20160225152828.jpg','<p>Solu&ccedil;&otilde;es Financeiras</p>\r\n\r\n<p><strong>Personalizadas</strong></p>\r\n','<p>Somos uma empresa de investimentos independente e, por isso, temos isen&ccedil;&atilde;o na escolha dos produtos que oferecemos, com transpar&ecirc;ncia, a nossos clientes.</p>\r\n\r\n<p>Nossa longa experi&ecirc;ncia no mercado financeiro &eacute; suportado por uma equipe t&eacute;cnica altamente qualificada e rigorosos padr&otilde;es de governan&ccedil;a e compliance.</p>\r\n\r\n<p>Com a integra&ccedil;&atilde;o desses fatores podemos analisar a situa&ccedil;&atilde;o financeira de cada cliente, recomendar o que realmente acreditamos ser a solu&ccedil;&atilde;o mais adequada e acompanhar com precis&atilde;o a evolu&ccedil;&atilde;o de seu patrim&ocirc;nio</p>\r\n','bg2_20160225152829.jpg','<p>Investimento de Capital</p>\r\n\r\n<p>com <strong>Seguran&ccedil;a</strong></p>\r\n','<p>Somos conservadores em rela&ccedil;&atilde;o ao investimento de capital dos nossos clientes.</p>\r\n\r\n<p>Acreditamos que um patrim&ocirc;nio com seriedade e dedica&ccedil;&atilde;o deve ser gerido do mesmo modo e por isso, trabahamos com &eacute;tica, dilig&ecirc;ncia e transpar&ecirc;ncia.<br />\r\nSomos comprometidos em rentabilizar os investimentos dos nossos clientes de forma consciente e fundamentada.</p>\r\n','bg7_20160225152830.jpg','<p>Reputa&ccedil;&atilde;o e <strong>Credibilidade</strong></p>\r\n','<p>Em mais de trinta anos de atua&ccedil;&atilde;o reunimos em nossa bagagem muita experi&ecirc;ncia, conhecimento t&eacute;cnico e, principalmente, o aprendizado adquirido com os constantes movimetnos do mercado financeiro. SOmos vitoriosos porque nunca abandonamos nossos ideais e assim seguimos independentes.</p>\r\n\r\n<p>Nossa credibilidade se mostra em relacionamentos longos, baseados em confian&ccedil;a, que nos endossam e geram novas e pr&oacute;speras rela&ccedil;&otilde;es.</p>\r\n','bg6_20160225152830.jpg','<p>Atendimento Qualificado</p>\r\n\r\n<p>e <strong>Comprometido</strong></p>\r\n','<p>Investimentos em treinamento e capacita&ccedil;&atilde;o, pois acreditamos que a excel&ecirc;ncia no atendimento &eacute; pe&ccedil;a chave para constru&ccedil;&atilde;o de relacionamentos duradouros.</p>\r\n\r\n<p>Valorizamos e reconhecemos nossos profissionais, pois sabemos que eles s&atilde;o a representa&ccedil;&atilde;o m&aacute;xima de nossa marca.</p>\r\n\r\n<p>&Eacute; assim que construimos o nosso modo de atender, visando sempre &agrave; satisfa&ccedil;&atilde;o do cliente e &agrave; seguran&ccedil;a do seu patrim&ocirc;nio.</p>\r\n','0000-00-00 00:00:00','2016-05-31 17:23:18');
/*!40000 ALTER TABLE `filosofia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_bg`
--

DROP TABLE IF EXISTS `home_bg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home_bg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_bg`
--

LOCK TABLES `home_bg` WRITE;
/*!40000 ALTER TABLE `home_bg` DISABLE KEYS */;
INSERT INTO `home_bg` VALUES (1,'bg1_20160225150832.jpg','0000-00-00 00:00:00','2016-05-31 16:58:54');
/*!40000 ALTER TABLE `home_bg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imprensa`
--

DROP TABLE IF EXISTS `imprensa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imprensa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto` text COLLATE utf8_unicode_ci,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imprensa`
--

LOCK TABLES `imprensa` WRITE;
/*!40000 ALTER TABLE `imprensa` DISABLE KEYS */;
INSERT INTO `imprensa` VALUES (1,'2016-05-31','Artigo de Exemplo','artigo-de-exemplo','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras semper lacus et eros eleifend, at convallis augue imperdiet. Quisque laoreet tempor nibh a blandit. Aliquam pharetra aliquet nunc, vel gravida nulla porta ac. Sed vitae vehicula nisi. In dapibus velit ac pulvinar auctor. Mauris eu dignissim libero, sit amet rhoncus sem. Etiam eget neque condimentum, auctor metus in, tempus nisi. Nulla tristique tortor nec ante vulputate feugiat. Aliquam nec pulvinar sapien.</p>\r\n\r\n<p><a href=\"#\">Link</a></p>\r\n\r\n<p>Aliquam placerat mauris nec ornare pellentesque. Pellentesque ac imperdiet felis. Quisque porttitor nibh ornare, ullamcorper lorem id, interdum neque. Donec tristique tristique bibendum. Aenean quis tristique tellus, vel malesuada augue. Maecenas mattis non lacus vitae auctor. Vivamus vitae nibh commodo ligula vehicula molestie. Donec lacus sem, eleifend eget tempus suscipit, eleifend vitae dolor. Duis imperdiet mi rutrum lacus ultrices, quis pulvinar felis vulputate. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus id ullamcorper lacus. Vestibulum at erat eget arcu tincidunt facilisis quis et enim. Phasellus semper urna pharetra blandit lacinia.</p>\r\n','20160531172444_exemplo.txt','youtube','BsogwgZJW_Q','2016-05-31 17:24:44','2016-05-31 17:24:44');
/*!40000 ALTER TABLE `imprensa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imprensa_bg`
--

DROP TABLE IF EXISTS `imprensa_bg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imprensa_bg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imprensa_bg`
--

LOCK TABLES `imprensa_bg` WRITE;
/*!40000 ALTER TABLE `imprensa_bg` DISABLE KEYS */;
INSERT INTO `imprensa_bg` VALUES (1,'bg3_20160225150602.jpg','0000-00-00 00:00:00','2016-05-31 17:08:15');
/*!40000 ALTER TABLE `imprensa_bg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imprensa_imagens`
--

DROP TABLE IF EXISTS `imprensa_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imprensa_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imprensa_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `imprensa_imagens_imprensa_id_foreign` (`imprensa_id`),
  CONSTRAINT `imprensa_imagens_imprensa_id_foreign` FOREIGN KEY (`imprensa_id`) REFERENCES `imprensa` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imprensa_imagens`
--

LOCK TABLES `imprensa_imagens` WRITE;
/*!40000 ALTER TABLE `imprensa_imagens` DISABLE KEYS */;
INSERT INTO `imprensa_imagens` VALUES (1,1,0,'bg1_20160531172554.jpg','2016-05-31 17:25:55','2016-05-31 17:25:55'),(2,1,0,'bg2_20160531172554.jpg','2016-05-31 17:25:55','2016-05-31 17:25:55');
/*!40000 ALTER TABLE `imprensa_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_02_23_040032_create_home_table',1),('2016_02_23_040151_create_somos_ativa_table',1),('2016_02_23_040413_create_filosofia_table',1),('2016_02_23_040830_create_ativa_wealth_table',1),('2016_02_23_040928_create_contato_table',1),('2016_02_23_144048_create_contatos_recebidos',1),('2016_02_25_124703_create_imprensa_table',1),('2016_02_25_124713_create_imprensa_imagens_table',1),('2016_03_31_175755_create_politicas_table',1),('2016_03_31_183109_create_documentos_tables',1),('2016_05_30_030250_create_carta_ativa_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `politicas`
--

DROP TABLE IF EXISTS `politicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `politicas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `politicas`
--

LOCK TABLES `politicas` WRITE;
/*!40000 ALTER TABLE `politicas` DISABLE KEYS */;
INSERT INTO `politicas` VALUES (1,'<h2 class=\"politica-titulo\">POL&Iacute;TICAS DO SITE E DE NEGOCIA&Ccedil;&Atilde;O</h2>\r\n\r\n<p>As informa&ccedil;&otilde;es contidas neste site s&atilde;o de car&aacute;ter meramente informativo, bem como n&atilde;o se trata de qualquer tipo de aconselhamento para a realiza&ccedil;&atilde;o de investimento, n&atilde;o devendo ser utilizadas com este prop&oacute;sito, nem entendidas como tal, inclusive em qualquer localidade ou jurisdi&ccedil;&atilde;o em que tal oferta, solicita&ccedil;&atilde;o ou venda possa ser contra lei. ATIVA WEALTH MANAGEMENT n&atilde;o comercializa, tampouco distribui cotas de fundos de investimentos ou qualquer outro ativo financeiro.</p>\r\n\r\n<p>Fundos de Investimento n&atilde;o contam com a garantia do administrador, do gestor da carteira, de qualquer mecanismo de seguro, ou, ainda do Fundo Garantidor de Cr&eacute;dito - FGC<br />\r\nAntes de tomar a decis&atilde;o de aplicar em qualquer opera&ccedil;&atilde;o, os potenciais investidores devem considerar cuidadosamente, tendo em vista suas pr&oacute;prias situa&ccedil;&otilde;es financeiras, seus objetivos de investimento, todas as informa&ccedil;&otilde;es dispon&iacute;veis e, em particular, avaliar os fatores de risco aos quais o investimento est&aacute; sujeito.</p>\r\n\r\n<p>As decis&otilde;es de investimento s&atilde;o de responsabilidade total e irrestrita do leitor. A ATIVA WEALTH MANAGEMENT n&atilde;o pode ser responsabilizada por preju&iacute;zos oriundos de decis&otilde;es tomadas com base nas informa&ccedil;&otilde;es aqui apresentadas.</p>\r\n\r\n<p>Para mais informa&ccedil;&otilde;es sobre os produtos relacionadas ao objetivo, riscos, rentabilidade dos &uacute;ltimos anos e outros, acesse o site www.ativainvestimentos.com.br ou entre em contato com a nossa Central de Atendimento: 0800 285 0147.</p>\r\n\r\n<p><strong>Ouvidoria: ATIVA 0800 282 9900.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2 class=\"politica-titulo\">POL&Iacute;TICA DE PRIVACIDADE E SEGURAN&Ccedil;A</h2>\r\n\r\n<p>A Ativa Corretora se compromete com a preserva&ccedil;&atilde;o m&aacute;xima da privacidade de seus clientes. Para manter esse compromisso, adotamos como Pol&iacute;tica de Privacidade n&atilde;o revelar ou divulgar quaisquer informa&ccedil;&otilde;es a respeito de seus usu&aacute;rios, sem a expressa autoriza&ccedil;&atilde;o dos mesmos, a n&atilde;o ser para:</p>\r\n\r\n<p>Cumprir exig&ecirc;ncia legal ou determina&ccedil;&atilde;o de autoridade competente;</p>\r\n\r\n<p>Fornecer dados estat&iacute;sticos, em car&aacute;ter gen&eacute;rico, sobre o acesso e a utiliza&ccedil;&atilde;o das informa&ccedil;&otilde;es disponibilizadas neste site.</p>\r\n\r\n<p>A Ativa poder&aacute; registrar e armazenar em meios magn&eacute;ticos pr&oacute;prios os atos que seus clientes praticarem atrav&eacute;s deste site. Tais dados poder&atilde;o ser utilizados como prova, sempre que necess&aacute;rio.</p>\r\n\r\n<p>Nosso modelo de seguran&ccedil;a garante a integridade dos bancos de dados e de todas as opera&ccedil;&otilde;es feitas pela ATIVA. Conhe&ccedil;a os principais itens:</p>\r\n\r\n<p><strong>Assinatura Eletr&ocirc;nica</strong><br />\r\n&Eacute; uma senha alfanum&eacute;rica, cadastrada pelo cliente em seus primeiros acessos ao site da ATIVA. A Assinatura Eletr&ocirc;nica assegura que s&oacute; voc&ecirc; confirma o envio de ordens e outras opera&ccedil;&otilde;es feitas online.</p>\r\n\r\n<p><strong>Conex&atilde;o segura SSL de 128 bits</strong><br />\r\nEm uma conex&atilde;o segura com um servidor na Internet, os dados transmitidos s&atilde;o criptografados. Desta forma, mesmo que algu&eacute;m consiga interceptar as mensagens, n&atilde;o ser&aacute; capaz de interpret&aacute;-las. Na ATIVA, utilizamos uma conex&atilde;o criptografada de 128 bits, a mais avan&ccedil;ada dispon&iacute;vel para o mercado corporativo. Nossos servidores tamb&eacute;m s&atilde;o certificados por uma ag&ecirc;ncia de auditoria independente, uma confirma&ccedil;&atilde;o da validade das conex&otilde;es codificadas.</p>\r\n\r\n<p><strong>Tempo de inatividade do sistema</strong><br />\r\nPara evitar acessos indesejados &agrave; sua tela de negocia&ccedil;&atilde;o, a ATIVA usa um mecanismo de interrup&ccedil;&atilde;o da conex&atilde;o. Sempre que voc&ecirc; passar mais de uma hora sem navegar em nosso site, a sess&atilde;o ser&aacute; encerrada. Fa&ccedil;a o login novamente quando quiser voltar a usar a p&aacute;gina.</p>\r\n\r\n<p><strong>Firewall</strong><br />\r\nA ATIVA segue o exemplo das maiores institui&ccedil;&otilde;es do mercado e impede qualquer acesso indevido &agrave;s informa&ccedil;&otilde;es dos servidores de aplica&ccedil;&atilde;o e bancos de dados.</p>\r\n\r\n<p><strong>Seguran&ccedil;a da senha do cliente</strong><br />\r\nAssim como sua Assinatura Eletr&ocirc;nica, sua senha &eacute; de uso exclusivo pessoal e intransfer&iacute;vel. A seguran&ccedil;a de suas informa&ccedil;&otilde;es depende do sigilo destes dados, por isso, nunca repasse sua Senha ou Assinatura Eletr&ocirc;nica a terceiros.</p>\r\n\r\n<p>Em caso de suspeita de viola&ccedil;&atilde;o de Senha ou Assinatura Eletr&ocirc;nica, entre em contato com o Atendimento pelo telefone: 4007 2447 para Capitais e Regi&otilde;es Metropolitanas e 0800 285 0147 para Demais Regi&otilde;es, para a altera&ccedil;&atilde;o de seus dados de acesso.</p>\r\n','bg9_20160331204023.jpg','0000-00-00 00:00:00','2016-05-31 17:09:45');
/*!40000 ALTER TABLE `politicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `somos_ativa`
--

DROP TABLE IF EXISTS `somos_ativa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `somos_ativa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `somos_ativa`
--

LOCK TABLES `somos_ativa` WRITE;
/*!40000 ALTER TABLE `somos_ativa` DISABLE KEYS */;
INSERT INTO `somos_ativa` VALUES (1,'<p>Os mais de 30 anos de tradi&ccedil;&atilde;o e excel&ecirc;ncia reconhecida no mercado financeiro deixam claro: Ativa &eacute; uma corretora que sempre soube ver al&eacute;m.<br />\r\nIsso significa olhar atrav&eacute;s de n&uacute;meros e valores, de investidores e seus patrim&ocirc;nios, e poder enxergar sonhos e horizontes, pessoas e suas hist&oacute;rias.<br />\r\nAtiva v&ecirc; todos do mundo dos investimentos e identifica o mais seguro para os clientes.</p>\r\n\r\n<p>Essa vis&atilde;o ampla foi a ideia central para a constru&ccedil;&atilde;o da marca ativa e de sua identidade.<br />\r\nCom a capacidade de ver o todo em um mercado de tantos altos e baixos - onde os altos s&atilde;o oportunidades para todos; e os baixos, oportunidades para quem sabe-, Ativa desenvolveu uma personalidade perspicaz e inteligente, que combina perspectiva &aacute; altura de sua experi&ecirc;ncia e revela cenas que traduzem a tranquilidade da seguran&ccedil;a financeira.</p>\r\n','youtube','BsogwgZJW_Q','bg6_20160225151250.jpg','0000-00-00 00:00:00','2016-05-31 16:52:49');
/*!40000 ALTER TABLE `somos_ativa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$IkxoPtJ/hCbSNPaEOQHJIOFucNonJT4IOZd7eB8cH0D5uMVRhRipS',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-31 17:26:50
