<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LinksRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'texto' => 'required',
            'link' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
