<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FilosofiaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'background_abertura' => 'image',
            'texto_abertura_1'    => 'required',
            'texto_abertura_2'    => 'required',
            'titulo_1'            => 'required',
            'texto_1'             => 'required',
            'background_1'        => 'image',
            'titulo_2'            => 'required',
            'texto_2'             => 'required',
            'background_2'        => 'image',
            'titulo_3'            => 'required',
            'texto_3'             => 'required',
            'background_3'        => 'image',
            'titulo_4'            => 'required',
            'texto_4'             => 'required',
            'background_4'        => 'image',
        ];
    }
}
