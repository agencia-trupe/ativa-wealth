<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RodapeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'endereco'   => 'required',
            'disclaimer' => '',
            'cor_fundo'  => 'required',
            'cor_texto'  => 'required',
        ];
    }
}
