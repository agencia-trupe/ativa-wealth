<?php

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('somos-ativa', ['as' => 'somos-ativa', 'uses' => 'SomosAtivaController@index']);
Route::get('filosofia/{passo?}', ['as' => 'filosofia', 'uses' => 'FilosofiaController@index']);
Route::get('ativa-wealth', ['as' => 'ativa-wealth', 'uses' => 'AtivaWealthController@index']);
Route::get('carta-ativa-wm', ['as' => 'carta-ativa-wm', 'uses' => 'CartaAtivaController@index']);
Route::get('imprensa', ['as' => 'imprensa', 'uses' => 'ImprensaController@index']);
Route::get('imprensa/{imprensa_slug}', ['as' => 'imprensa.show', 'uses' => 'ImprensaController@show']);
Route::get('politicas', ['as' => 'politicas', 'uses' => 'PoliticasController@index']);
Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);
Route::post('contato', ['as' => 'contato.envio', 'uses' => 'ContatoController@envio']);


// Painel
Route::group(['prefix' => 'painel', 'namespace' => 'Painel', 'middleware' => 'auth'], function() {
    Route::get('/', ['as' => 'painel', 'uses' => 'HomeController@index']);

    Route::patch('home/background/{home_bg}', [
        'as'   => 'painel.home.background',
        'uses' => 'HomePageController@updateBg'
    ]);
    Route::resource('home', 'HomePageController');

    Route::resource('somos-ativa', 'SomosAtivaController', ['only' => ['index', 'update']]);
    Route::resource('filosofia', 'FilosofiaController', ['only' => ['index', 'update']]);
    Route::resource('ativa-wealth', 'AtivaWealthController', ['only' => ['index', 'update']]);

    Route::patch('imprensa/background/{imprensa_bg}', [
        'as'   => 'painel.imprensa.background',
        'uses' => 'ImprensaController@updateBg'
    ]);
    Route::resource('imprensa', 'ImprensaController');
    Route::get('imprensa/{imprensa}/imagens/clear', [
        'as'   => 'painel.imprensa.imagens.clear',
        'uses' => 'ImprensaImagensController@clear'
    ]);
    Route::resource('imprensa.imagens', 'ImprensaImagensController');

    Route::resource('politicas', 'PoliticasController', ['only' => ['index', 'update']]);
    Route::resource('documentos/categorias', 'DocumentosCategoriasController');
    Route::resource('documentos', 'DocumentosController');

    Route::patch('carta-ativa-wm/background/{cartaAtiva_bg}', [
        'as'   => 'painel.carta-ativa-wm.background',
        'uses' => 'CartaAtivaController@updateBg'
    ]);
    Route::resource('carta-ativa-wm', 'CartaAtivaController');

    Route::resource('contato/recebidos', 'ContatosRecebidosController');
    Route::resource('contato', 'ContatoController', ['only' => ['index', 'update']]);

    Route::resource('usuarios', 'UsuariosController');

    Route::post('order', 'HomeController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


// Auth
Route::group(['prefix' => 'painel', 'namespace' => 'Auth'], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
});
