<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Imprensa;
use App\Models\ImprensaBg;

class ImprensaController extends Controller
{
    public function index()
    {
        $artigos    = Imprensa::ordenados()->paginate(6);
        $background = ImprensaBg::first();

        return view('frontend.imprensa.index', compact('artigos', 'background'));
    }

    public function show(Imprensa $artigo)
    {
        $artigo->load('imagens');
        $background = ImprensaBg::first();

        return view('frontend.imprensa.show', compact('artigo', 'background'));
    }
}
