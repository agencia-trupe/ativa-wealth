<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CartaAtiva;
use App\Models\CartaAtivaBg;

class CartaAtivaController extends Controller
{
    public function index()
    {
        $arquivos = CartaAtiva::ordenados()->paginate(8);
        $background = CartaAtivaBg::first();

        return view('frontend.carta-ativa-wm', compact('arquivos', 'background'));
    }
}
