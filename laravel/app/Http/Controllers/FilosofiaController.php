<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Filosofia;

class FilosofiaController extends Controller
{
    public function index($passo = null)
    {
        $passos   = range(1,4);
        $conteudo = Filosofia::first();

        if (! $passo) return view('frontend.filosofia.index', compact('conteudo'));

        if (! in_array($passo, $passos)) return abort(404);

        $proximo = $passo + 1;
        $proximo = in_array($proximo, $passos) ? $proximo : null;

        $anterior = $passo - 1;
        $anterior = in_array($anterior, $passos) ? $anterior : null;

        return view('frontend.filosofia.passo', compact('conteudo', 'passo', 'proximo', 'anterior'));
    }
}
