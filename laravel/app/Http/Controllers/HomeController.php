<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\HomeBg;
use App\Models\Banner;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();
        $background = HomeBg::first();

        return view('frontend.home', compact('banners', 'background'));
    }
}
