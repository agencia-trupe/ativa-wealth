<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ImprensaRequest;
use App\Http\Requests\ImprensaBgRequest;
use App\Http\Controllers\Controller;

use App\Models\Imprensa;
use App\Models\ImprensaBg;

use App\Helpers\CropImage;

class ImprensaController extends Controller
{
    public function index()
    {
        $imprensa    = Imprensa::ordenados()->paginate(20);
        $imprensa_bg = ImprensaBg::first();

        return view('painel.imprensa.index', compact('imprensa', 'imprensa_bg'));
    }

    public function create()
    {
        return view('painel.imprensa.create');
    }

    public function store(ImprensaRequest $request)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            if (isset($input['arquivo'])) {
                $input['arquivo'] = $this->uploadArquivo($input['arquivo']);
            }

            Imprensa::create($input);
            return redirect()->route('painel.imprensa.index')->with('success', 'Artigo adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar artigo: '.$e->getMessage()]);

        }
    }

    public function edit(Imprensa $imprensa)
    {
        return view('painel.imprensa.edit', compact('categorias', 'imprensa'));
    }

    public function update(ImprensaRequest $request, Imprensa $imprensa)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            if (isset($input['arquivo'])) {
                $input['arquivo'] = $this->uploadArquivo($input['arquivo']);
            }
            if (! isset($input['video_codigo'])) {
                $input['video_codigo'] = null;
            }

            $imprensa->update($input);
            return redirect()->route('painel.imprensa.index')->with('success', 'Artigo alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar artigo: '.$e->getMessage()]);

        }
    }

    public function destroy(Imprensa $imprensa)
    {
        try {

            $imprensa->imagens()->delete();
            $imprensa->delete();
            return redirect()->route('painel.imprensa.index')->with('success', 'Artigo excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir artigo: '.$e->getMessage()]);

        }
    }

    public function uploadArquivo($file)
    {
        $filePath = 'assets/arquivos/';
        $fileName = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $file->getClientOriginalName());
        $fileName = date('YmdHis').'_'.$fileName;

        $file->move(public_path($filePath), $fileName);
        return $fileName;
    }

    public function updateBg(ImprensaBgRequest $request, ImprensaBg $imprensa_bg)
    {
        try {

            $input = array_filter($request->except(['_token', '_method']), 'strlen');

            if (isset($input['background'])) {
                $input['background'] = CropImage::make('background', [
                    'width'  => 1980,
                    'height' => null,
                    'upsize' => true,
                    'path'   => 'assets/img/backgrounds/'
                ]);
            }

            $imprensa_bg->update($input);
            return back()->with('success', 'Background alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar background: '.$e->getMessage()]);

        }
    }
}
