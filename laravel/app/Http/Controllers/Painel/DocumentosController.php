<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DocumentoRequest;
use App\Http\Controllers\Controller;

use App\Models\DocumentoCategoria;
use App\Models\Documento;

class DocumentosController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = DocumentoCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (DocumentoCategoria::find($filtro)) {
            $documentos = Documento::ordenados()->categoria($filtro)->get();
        } else {
            $documentos = Documento::join('documentos_categorias as cat', 'cat.id', '=', 'documentos_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('documentos.*')
                ->ordenados()->get();
        }

        return view('painel.documentos.index', compact('categorias', 'documentos', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.documentos.create', compact('categorias'));
    }

    public function store(DocumentoRequest $request)
    {
        try {

            $input = $request->all();
            $input['arquivo'] = $this->uploadArquivo($input['arquivo']);

            Documento::create($input);
            return redirect()->route('painel.documentos.index')->with('success', 'Documento adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar documento: '.$e->getMessage()]);

        }
    }

    public function edit(Documento $documento)
    {
        $categorias = $this->categorias;

        return view('painel.documentos.edit', compact('categorias', 'documento'));
    }

    public function update(DocumentoRequest $request, Documento $documento)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            if (isset($input['arquivo'])) {
                $input['arquivo'] = $this->uploadArquivo($input['arquivo']);
            }

            $documento->update($input);
            return redirect()->route('painel.documentos.index')->with('success', 'Documento alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar documento: '.$e->getMessage()]);

        }
    }

    public function destroy(Documento $documento)
    {
        try {

            $documento->delete();
            return redirect()->route('painel.documentos.index')->with('success', 'Documento excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir documento: '.$e->getMessage()]);

        }
    }

    public function uploadArquivo($file)
    {
        $filePath = 'assets/documentos/';
        $fileName = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move(public_path($filePath), $fileName);
        return $fileName;
    }
}
