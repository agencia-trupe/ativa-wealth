<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ImprensaImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Imprensa;
use App\Models\ImprensaImagem;
use App\Helpers\CropImage;

class ImprensaImagensController extends Controller
{
    private $image_config = [
        [
            'width'   => 95,
            'height'  => 95,
            'path'    => 'assets/img/imprensa/imagens/thumbs/'
        ],
        [
            'width'   => 980,
            'height'  => null,
            'upsize'  => true,
            'path'    => 'assets/img/imprensa/imagens/'
        ]
    ];

    public function index(Imprensa $imprensa)
    {
        $imagens = ImprensaImagem::imprensa($imprensa->id)->ordenados()->get();

        return view('painel.imprensa.imagens.index', compact('imagens', 'imprensa'));
    }

    public function show(Imprensa $imprensa, ImprensaImagem $imagem)
    {
        return $imagem;
    }

    public function create(Imprensa $imprensa)
    {
        return view('painel.imprensa.imagens.create', compact('imprensa'));
    }

    public function store(Imprensa $imprensa, ImprensaImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            $input['imprensa_id'] = $imprensa->id;

            $imagem = ImprensaImagem::create($input);
            $view = view('painel.imprensa.imagens.imagem', compact('imprensa', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Imprensa $imprensa, ImprensaImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.imprensa.imagens.index', $imprensa)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Imprensa $imprensa)
    {
        try {

            $imprensa->imagens()->delete();
            return redirect()->route('painel.imprensa.imagens.index', $imprensa)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
