<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\FilosofiaRequest;
use App\Http\Controllers\Controller;

use App\Models\Filosofia;
use App\Helpers\CropImage;

class FilosofiaController extends Controller
{
    private $image_config = [
        'background' => [
            'width'  => 1980,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/backgrounds/'
        ],
        'imagem' => [
            'width'  => 600,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/filosofia/'
        ]
    ];

    public function index()
    {
        $registro = Filosofia::first();

        return view('painel.filosofia.index', compact('registro'));
    }

    public function update(FilosofiaRequest $request, Filosofia $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            if (isset($input['background_abertura'])) {
                $input['background_abertura'] = CropImage::make('background_abertura', $this->image_config['background']);
            }
            if (isset($input['background_1'])) {
                $input['background_1'] = CropImage::make('background_1', $this->image_config['background']);
            }
            if (isset($input['background_2'])) {
                $input['background_2'] = CropImage::make('background_2', $this->image_config['background']);
            }
            if (isset($input['background_3'])) {
                $input['background_3'] = CropImage::make('background_3', $this->image_config['background']);
            }
            if (isset($input['background_4'])) {
                $input['background_4'] = CropImage::make('background_4', $this->image_config['background']);
            }

            if (isset($input['imagem_abertura'])) {
                $input['imagem_abertura'] = CropImage::make('imagem_abertura', $this->image_config['imagem']);
            }
            if (isset($input['imagem_1'])) {
                $input['imagem_1'] = CropImage::make('imagem_1', $this->image_config['imagem']);
            }
            if (isset($input['imagem_2'])) {
                $input['imagem_2'] = CropImage::make('imagem_2', $this->image_config['imagem']);
            }
            if (isset($input['imagem_3'])) {
                $input['imagem_3'] = CropImage::make('imagem_3', $this->image_config['imagem']);
            }
            if (isset($input['imagem_4'])) {
                $input['imagem_4'] = CropImage::make('imagem_4', $this->image_config['imagem']);
            }

            $registro->update($input);
            return redirect()->route('painel.filosofia.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }
}
