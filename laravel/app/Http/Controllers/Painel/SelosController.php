<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SelosRequest;
use App\Http\Controllers\Controller;

use App\Models\Selo;

class SelosController extends Controller
{
    public function index()
    {
        $registros = Selo::ordenados()->get();

        return view('painel.selos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.selos.create');
    }

    public function store(SelosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Selo::upload_imagem();

            Selo::create($input);

            return redirect()->route('painel.selos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Selo $registro)
    {
        return view('painel.selos.edit', compact('registro'));
    }

    public function update(SelosRequest $request, Selo $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Selo::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.selos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Selo $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.selos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
