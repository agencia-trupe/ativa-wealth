<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\RodapeRequest;
use App\Http\Controllers\Controller;

use App\Models\Rodape;

class RodapeController extends Controller
{
    public function index()
    {
        $registro = Rodape::first();

        return view('painel.rodape.edit', compact('registro'));
    }

    public function update(RodapeRequest $request, Rodape $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.rodape.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
