<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PoliticasRequest;
use App\Http\Controllers\Controller;

use App\Models\Politicas;
use App\Helpers\CropImage;

class PoliticasController extends Controller
{
    private $image_config = [
        'width'  => 1980,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/backgrounds/'
    ];

    public function index()
    {
        $registro = Politicas::first();

        return view('painel.politicas.index', compact('registro'));
    }

    public function update(PoliticasRequest $request, Politicas $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['background'])) {
                $input['background'] = CropImage::make('background', $this->image_config);
            }

            $registro->update($input);
            return redirect()->route('painel.politicas.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }
}
