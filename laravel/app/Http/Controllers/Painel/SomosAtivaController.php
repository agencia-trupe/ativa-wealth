<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SomosAtivaRequest;
use App\Http\Controllers\Controller;

use App\Models\SomosAtiva;
use App\Helpers\CropImage;

class SomosAtivaController extends Controller
{
    private $image_config = [
        'width'  => 1980,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/backgrounds/'
    ];

    public function index()
    {
        $registro = SomosAtiva::first();

        return view('painel.somos-ativa.index', compact('registro'));
    }

    public function update(SomosAtivaRequest $request, SomosAtiva $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['background'])) {
                $input['background'] = CropImage::make('background', $this->image_config);
            }

            $registro->update($input);
            return redirect()->route('painel.somos-ativa.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }
}
