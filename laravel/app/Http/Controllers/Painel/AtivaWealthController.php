<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AtivaWealthRequest;
use App\Http\Controllers\Controller;

use App\Models\AtivaWealth;
use App\Helpers\CropImage;

class AtivaWealthController extends Controller
{
    private $image_config = [
        'width'  => 1980,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/backgrounds/'
    ];

    public function index()
    {
        $registro = AtivaWealth::first();

        return view('painel.ativa-wealth.index', compact('registro'));
    }

    public function update(AtivaWealthRequest $request, AtivaWealth $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['background'])) {
                $input['background'] = CropImage::make('background', $this->image_config);
            }

            $registro->update($input);
            return redirect()->route('painel.ativa-wealth.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }
}
