<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DocumentoCategoriaRequest;
use App\Http\Controllers\Controller;

use App\Models\DocumentoCategoria;

class DocumentosCategoriasController extends Controller
{
    public function index()
    {
        $categorias = DocumentoCategoria::ordenados()->get();

        return view('painel.documentos.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.documentos.categorias.create');
    }

    public function store(DocumentoCategoriaRequest $request)
    {
        try {

            $input = $request->all();

            DocumentoCategoria::create($input);
            return redirect()->route('painel.documentos.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(DocumentoCategoria $categoria)
    {
        return view('painel.documentos.categorias.edit', compact('categoria'));
    }

    public function update(DocumentoCategoriaRequest $request, DocumentoCategoria $categoria)
    {
        try {

            $input = $request->all();

            $categoria->update($input);
            return redirect()->route('painel.documentos.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(DocumentoCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.documentos.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
