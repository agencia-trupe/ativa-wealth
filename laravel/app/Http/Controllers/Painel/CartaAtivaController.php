<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CartaAtivaRequest;
use App\Http\Requests\CartaAtivaBgRequest;
use App\Http\Controllers\Controller;

use App\Models\CartaAtiva;
use App\Models\CartaAtivaBg;

use App\Helpers\CropImage;

class CartaAtivaController extends Controller
{
    public function index()
    {
        $arquivos = CartaAtiva::ordenados()->paginate(20);
        $cartaAtiva_background = CartaAtivaBg::first();

        return view('painel.carta-ativa-wm.index', compact('arquivos', 'cartaAtiva_background'));
    }

    public function create()
    {
        return view('painel.carta-ativa-wm.create');
    }

    public function store(CartaAtivaRequest $request)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            $input['arquivo'] = $this->uploadArquivo($input['arquivo']);

            CartaAtiva::create($input);
            return redirect()->route('painel.carta-ativa-wm.index')->with('success', 'Arquivo adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar arquivo: '.$e->getMessage()]);

        }
    }

    public function edit(CartaAtiva $arquivo)
    {
        return view('painel.carta-ativa-wm.edit', compact('arquivo'));
    }

    public function update(CartaAtivaRequest $request, CartaAtiva $arquivo)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            if (isset($input['arquivo'])) {
                $input['arquivo'] = $this->uploadArquivo($input['arquivo']);
            }

            $arquivo->update($input);
            return redirect()->route('painel.carta-ativa-wm.index')->with('success', 'Arquivo alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar arquivo: '.$e->getMessage()]);

        }
    }

    public function destroy(CartaAtiva $arquivo)
    {
        try {

            $arquivo->delete();
            return redirect()->route('painel.carta-ativa-wm.index')->with('success', 'Arquivo excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir arquivo: '.$e->getMessage()]);

        }
    }

    public function uploadArquivo($file)
    {
        $filePath = 'assets/arquivos/';
        $fileName = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $file->getClientOriginalName());
        $fileName = date('YmdHis').'_'.$fileName;

        $file->move(public_path($filePath), $fileName);
        return $fileName;
    }

    public function updateBg(CartaAtivaBgRequest $request, CartaAtivaBg $cartaAtiva_bg)
    {
        try {

            $input = array_filter($request->except(['_token', '_method']), 'strlen');

            if (isset($input['background'])) {
                $input['background'] = CropImage::make('background', [
                    'width'  => 1980,
                    'height' => null,
                    'upsize' => true,
                    'path'   => 'assets/img/backgrounds/'
                ]);
            }

            $cartaAtiva_bg->update($input);
            return back()->with('success', 'Background alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar background: '.$e->getMessage()]);

        }
    }
}
