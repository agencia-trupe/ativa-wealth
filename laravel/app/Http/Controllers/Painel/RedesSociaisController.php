<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\RedesSociaisRequest;
use App\Http\Controllers\Controller;

use App\Models\RedeSocial;

class RedesSociaisController extends Controller
{
    public function index()
    {
        $registros = RedeSocial::ordenados()->get();

        return view('painel.redes-sociais.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.redes-sociais.create');
    }

    public function store(RedesSociaisRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = RedeSocial::upload_imagem();

            RedeSocial::create($input);

            return redirect()->route('painel.redes-sociais.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(RedeSocial $registro)
    {
        return view('painel.redes-sociais.edit', compact('registro'));
    }

    public function update(RedesSociaisRequest $request, RedeSocial $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = RedeSocial::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.redes-sociais.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(RedeSocial $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.redes-sociais.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
