<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\BannerRequest;
use App\Http\Requests\HomeBgRequest;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\HomeBg;

use App\Helpers\CropImage;

class HomePageController extends Controller
{
    private $image_config = [
        'width'  => 560,
        'height' => 350,
        'path'   => 'assets/img/banners/'
    ];

    public function index()
    {
        $banners = Banner::ordenados()->get();
        $home_bg = HomeBg::first();

        return view('painel.home.index', compact('banners', 'home_bg'));
    }

    public function create()
    {
        return view('painel.home.create');
    }

    public function store(BannerRequest $request)
    {
        if (Banner::count() >= 6) {
            return redirect()->route('painel.home.index')->withErrors(['Erro ao adicionar banner: limite de registros excedido.']);
        }

        try {

            $input = array_filter($request->all(), 'strlen');
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Banner::create($input);
            return redirect()->route('painel.home.index')->with('success', 'Banner adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar banner: '.$e->getMessage()]);

        }
    }

    public function edit(Banner $banner)
    {
        return view('painel.home.edit', compact('categorias', 'banner'));
    }

    public function update(BannerRequest $request, Banner $banner)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            if (isset($input['imagem'])) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            }

            $banner->update($input);
            return redirect()->route('painel.home.index')->with('success', 'Banner alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar banner: '.$e->getMessage()]);

        }
    }

    public function destroy(Banner $banner)
    {
        try {

            $banner->delete();
            return redirect()->route('painel.home.index')->with('success', 'Banner excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir banner: '.$e->getMessage()]);

        }
    }

    public function uploadArquivo($file)
    {
        $filePath = 'assets/arquivos/';
        $fileName = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $file->getClientOriginalName());
        $fileName = date('YmdHis').'_'.$fileName;

        $file->move(public_path($filePath), $fileName);
        return $fileName;
    }

    public function updateBg(HomeBgRequest $request, HomeBg $home_bg)
    {
        try {

            $input = array_filter($request->except(['_token', '_method']), 'strlen');

            if (isset($input['background'])) {
                $input['background'] = CropImage::make('background', [
                    'width'  => 1980,
                    'height' => null,
                    'upsize' => true,
                    'path'   => 'assets/img/backgrounds/'
                ]);
            }

            $home_bg->update($input);
            return back()->with('success', 'Background alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar background: '.$e->getMessage()]);

        }
    }
}
