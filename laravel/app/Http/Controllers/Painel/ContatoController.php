<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ContatoRequest;
use App\Http\Controllers\Controller;

use App\Models\Contato;
use App\Helpers\CropImage;

class ContatoController extends Controller
{
    private $image_config = [
        'width'  => 1980,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/backgrounds/'
    ];

    public function index()
    {
        $registro = Contato::first();

        return view('painel.contato.index', compact('registro'));
    }

    public function update(ContatoRequest $request, Contato $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['background'])) {
                $input['background'] = CropImage::make('background', $this->image_config);
            }

            $registro->update($input);
            return redirect()->route('painel.contato.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }
}
