<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\SomosAtiva;

class SomosAtivaController extends Controller
{
    public function index()
    {
        $conteudo = SomosAtiva::first();

        return view('frontend.somos-ativa', compact('conteudo'));
    }
}
