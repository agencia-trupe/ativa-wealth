<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\AtivaWealth;

class AtivaWealthController extends Controller
{
    public function index()
    {
        $conteudo = AtivaWealth::first();

        return view('frontend.ativa-wealth', compact('conteudo'));
    }
}
