<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Politicas;
use App\Models\DocumentoCategoria;

class PoliticasController extends Controller
{
    public function index()
    {
        $conteudo   = Politicas::first();
        $documentos = DocumentoCategoria::with('documentos')->ordenados()->get();

        return view('frontend.politicas', compact('conteudo', 'documentos'));
    }
}
