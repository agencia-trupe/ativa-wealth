<?php

namespace App\Helpers;

class Tools
{

    public static function formataData($data)
    {
        $meses = ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'];

        list($mes, $ano) = explode('/', $data);

        return $meses[(int) $mes - 1] . ' ' . $ano;
    }

}
