<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('home', 'App\Models\Banner');
        $router->model('somos-ativa', 'App\Models\SomosAtiva');
        $router->model('filosofia', 'App\Models\Filosofia');
        $router->model('ativa-wealth', 'App\Models\AtivaWealth');
        $router->model('imprensa', 'App\Models\Imprensa');
        $router->model('imagens', 'App\Models\ImprensaImagem');
        $router->model('carta-ativa-wm', 'App\Models\CartaAtiva');
        $router->model('politicas', 'App\Models\Politicas');
        $router->model('documentos', 'App\Models\Documento');
        $router->model('categorias', 'App\Models\DocumentoCategoria');
        $router->model('contato', 'App\Models\Contato');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('imprensa_slug', function($value) {
            return \App\Models\Imprensa::with('imagens')->slug($value)->first() ?: \App::abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
