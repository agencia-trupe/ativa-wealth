<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Politicas extends Model
{
    protected $table = 'politicas';

    protected $guarded = ['id'];
}
