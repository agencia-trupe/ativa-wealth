<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentoCategoria extends Model
{
    protected $table = 'documentos_categorias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function documentos()
    {
        return $this->hasMany('App\Models\Documento', 'documentos_categoria_id')->ordenados();
    }
}
