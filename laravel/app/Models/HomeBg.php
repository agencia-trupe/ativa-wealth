<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeBg extends Model
{
    protected $table = 'home_bg';

    protected $guarded = ['id'];
}
