<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImprensaImagem extends Model
{
    protected $table = 'imprensa_imagens';

    protected $guarded = ['id'];

    public function scopeImprensa($query, $id)
    {
        return $query->where('imprensa_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
