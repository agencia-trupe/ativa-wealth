<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Rodape extends Model
{
    protected $table = 'rodape';

    protected $guarded = ['id'];

}
