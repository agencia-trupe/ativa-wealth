<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartaAtivaBg extends Model
{
    protected $table = 'carta_ativa_bg';

    protected $guarded = ['id'];
}
