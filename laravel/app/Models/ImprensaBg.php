<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImprensaBg extends Model
{
    protected $table = 'imprensa_bg';

    protected $guarded = ['id'];
}
