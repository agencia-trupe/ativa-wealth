<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SomosAtiva extends Model
{
    protected $table = 'somos_ativa';

    protected $guarded = ['id'];
}
