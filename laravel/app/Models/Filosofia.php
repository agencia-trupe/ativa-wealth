<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Filosofia extends Model
{
    protected $table = 'filosofia';

    protected $guarded = ['id'];
}
