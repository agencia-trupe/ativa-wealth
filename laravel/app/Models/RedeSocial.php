<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class RedeSocial extends Model
{
    protected $table = 'redes_sociais';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 24,
            'height' => 24,
            'path'   => 'assets/img/redes-sociais/'
        ]);
    }
}
