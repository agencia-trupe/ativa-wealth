<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AtivaWealth extends Model
{
    protected $table = 'ativa_wealth';

    protected $guarded = ['id'];
}
