@extends('frontend.common.template', [
    'background' => $background->background
])

@section('content')

    <div class="main imprensa-show">
        <h2>Imprensa</h2>

        <h1>{{ $artigo->titulo }}</h1>

        <div class="texto @if(! $artigo->arquivo && ! $artigo->video_codigo) full-width @endif">
            {!! $artigo->texto !!}

            @if(count($artigo->imagens))
            <div class="galeria">
                @foreach($artigo->imagens as $imagem)
                <a href="{{ asset('assets/img/imprensa/imagens/'.$imagem->imagem) }}" class="galeria-thumb" rel="galeria">
                    <img src="{{ asset('assets/img/imprensa/imagens/thumbs/'.$imagem->imagem) }}" alt="">
                </a>
                @endforeach
            </div>
            @endif
        </div>

        @if($artigo->arquivo || $artigo->video_codigo)
        <div class="aside">
            @if($artigo->arquivo)
            <a href="{{ asset('assets/arquivos/'.$artigo->arquivo) }}" target="_blank" class="arquivo">
                Download do conteúdo
            </a>
            @endif

            @if($artigo->video_codigo)
            <div class="video">
                @include('frontend.common.video', [
                    'tipo'   => $artigo->video_tipo,
                    'codigo' => $artigo->video_codigo
                ])
            </div>
            @endif
        </div>
        @endif
    </div>

@endsection
