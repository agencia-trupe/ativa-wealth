@extends('frontend.common.template', [
    'background' => $background->background
])

@section('content')

    <div class="main imprensa-index">
        <h2>Imprensa</h2>

        @foreach($artigos as $artigo)
        <a href="{{ route('imprensa.show', $artigo->slug) }}">
            <span>{{ $artigo->data }}</span>
            {{ $artigo->titulo }}
        </a>
        @endforeach

        {!! str_replace('/?', '?', $artigos->render()) !!}
    </div>

@endsection
