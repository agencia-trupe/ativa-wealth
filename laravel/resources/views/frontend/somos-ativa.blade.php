@extends('frontend.common.template', [
    'background' => $conteudo->background
])

@section('content')

    <div class="main ativa-wealth institucional">
        <div class="texto @if(! $conteudo->video_codigo) full-width @endif">
            <h1>Somos <strong>ATIVA</strong></h1>
            {!! $conteudo->texto !!}
        </div>

        @if($conteudo->video_codigo)
        <div class="video">
            @include('frontend.common.video', [
                'tipo'   => $conteudo->video_tipo,
                'codigo' => $conteudo->video_codigo
            ])
        </div>
        @endif
    </div>

@endsection
