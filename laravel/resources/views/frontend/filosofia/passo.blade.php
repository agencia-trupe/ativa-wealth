@extends('frontend.common.template', [
    'background' => $conteudo->{'background_'.$passo}
])

@section('content')

    <div class="main filosofia-passo">
        @if($anterior)
        <a href="{{ route('filosofia', $anterior) }}" class="btn-prev">anterior</a>
        @elseif($passo == 1)
        <a href="{{ route('filosofia') }}" class="btn-prev">anterior</a>
        @endif

        <div class="texto">
            <h2>Filosofia</h2>
            <h1>
                {!! $conteudo->{'titulo_'.$passo} !!}
            </h1>
            {!! $conteudo->{'texto_'.$passo} !!}
        </div>

        <div class="infografico">
            <img src="{{ asset('assets/img/layout/infografico_'.$passo.'.png') }}" alt="">
        </div>

        @if($proximo)
        <a href="{{ route('filosofia', $proximo) }}" class="btn-next">próximo</a>
        @endif
    </div>

@endsection
