@extends('frontend.common.template', [
    'background' => $conteudo->background_abertura
])

@section('content')

    <div class="main filosofia-index">
        <div class="texto">
            <h2>Filosofia</h2>
            {!! $conteudo->texto_abertura_1 !!}
            <div class="texto-secundario">
                {!! $conteudo->texto_abertura_2 !!}
            </div>
        </div>

        <div class="infografico">
            <img src="{{ asset('assets/img/layout/infografico_abertura.png') }}" alt="">
        </div>

        <a href="{{ route('filosofia', 1) }}" class="btn-next">próximo</a>
    </div>

@endsection
