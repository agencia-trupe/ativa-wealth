@extends('frontend.common.template', [
    'background' => $background->background
])

@section('content')

    <div class="main home">
        <div class="banners">
            @foreach($banners as $banner)
            <a href="{{ $banner->link }}" class="slide">
                <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
                <div class="chamada">
                    <h2>{{ $banner->titulo }}</h2>
                    <p>{{ $banner->descricao }}</p>
                </div>
            </a>
            @endforeach

            <div class="cycle-pager"></div>
        </div>
    </div>

@endsection
