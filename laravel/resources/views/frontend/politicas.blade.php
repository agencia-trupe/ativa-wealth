@extends('frontend.common.template', [
    'background' => $conteudo->background
])

@section('content')

    <div class="main politicas institucional">
        <div class="texto">
            {!! $conteudo->texto !!}
        </div>

        <div class="documentos">
            @foreach($documentos as $categoria)
                @if(count($categoria->documentos))
                    <h2>{{ $categoria->titulo }}</h2>
                    @foreach($categoria->documentos as $documento)
                        <a href="{{ url('assets/documentos/'.$documento->arquivo) }}" target="_blank">
                            &raquo; {{ $documento->titulo }}
                        </a>
                    @endforeach
                @endif
            @endforeach
        </div>
    </div>

@endsection