@extends('frontend.common.template', [
    'background' => $conteudo->background
])

@section('content')

    <div class="main contato">
        <h1>Contato</h1>

        <div class="row first">
            <form action="" id="form-contato" method="POST">
                <input type="text" name="nome" id="nome" placeholder="nome" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="telefone">
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <div id="form-contato-response"></div>
                <input type="submit" value="Enviar">
            </form>

            <div class="contato-col">
                <h3>São Paulo</h3>
                {!! $conteudo->saopaulo !!}
            </div>
        </div>

        <div class="row">
            @foreach([
                'riodejaneiro'  => 'Rio de Janeiro',
                'curitiba'      => 'Curitiba',
                'portoalegre'   => 'Porto Alegre',
                'belohorizonte' => 'Belo Horizonte'
            ] as $key => $value)
            <div class="contato-col">
                <h3>{{ $value }}</h3>
                {!! $conteudo->{$key} !!}
            </div>
            @endforeach
        </div>
    </div>

@endsection
