@extends('frontend.common.template', [
    'background' => $background->background
])

@section('content')

    <div class="main carta-ativa-wm">
        <h1>Carta ATIVA WM</h1>

        @foreach($arquivos as $arquivo)
        <a href="{{ asset('assets/arquivos/'.$arquivo->arquivo) }}" target="_blank">
            <div class="data">{{ Tools::formataData($arquivo->data) }}</div>
            <div class="titulo">{{ $arquivo->titulo }}</div>
        </a>
        @endforeach

        {!! str_replace('/?', '?', $arquivos->render()) !!}
    </div>

@endsection
