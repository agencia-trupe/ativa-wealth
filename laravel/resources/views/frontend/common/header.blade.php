    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="header-logo">{{ config('site.name') }}</a>

            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>

            <a href="https://saas.britech.com.br/ativawm/Login/LoginInit.aspx?ReturnUrl=%2fativawm%2fdefault.aspx" target="_blank" class="btn-login">Login</a>
        </div>
    </header>

    <nav>
        <a href="{{ route('home') }}" @if(str_is('home', Route::currentRouteName())) class="active" @endif>Home</a>
        <a href="{{ route('somos-ativa') }}" @if(str_is('somos-ativa*', Route::currentRouteName())) class="active" @endif>Somos <strong>ATIVA</strong></a>
        <a href="{{ route('filosofia') }}" @if(str_is('filosofia*', Route::currentRouteName())) class="active" @endif>Filosofia</a>
        <a href="{{ route('ativa-wealth') }}" @if(str_is('ativa-wealth*', Route::currentRouteName())) class="active" @endif>Ativa <strong>Wealth</strong></a>
        <a href="{{ route('imprensa') }}" @if(str_is('imprensa*', Route::currentRouteName())) class="active" @endif>Imprensa</a>
        <a href="{{ route('carta-ativa-wm') }}" @if(str_is('carta-ativa-wm*', Route::currentRouteName())) class="active" @endif>Carta ATIVA WM</a>
        <a href="{{ route('contato') }}" @if(str_is('contato*', Route::currentRouteName())) class="active" @endif>Contato</a>
    </nav>

    <div class="background" style="background-image:url('{{ asset('assets/img/backgrounds/'.$background) }}')"></div>
