    <footer>
        <div class="center">
            <div class="left">
                <p>© {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados.</p>
                <img src="{{ asset('assets/img/layout/selo-anbima.jpg') }}" alt="">
            </div>
            <div class="right">
                <a href="{{ route('politicas') }}" class="politicas">
                    Políticas do Site e de Negociação
                </a>

                @if($contato->facebook || $contato->twitter)
                <div class="social">
                    @if($contato->facebook)
                    <a href="{{ $contato->facebook }}" class="facebook">facebook</a>
                    @endif
                    @if($contato->twitter)
                    <a href="{{ $contato->twitter }}" class="twitter">twitter</a>
                    @endif
                </div>
                @endif
            </div>
        </div>
    </footer>
