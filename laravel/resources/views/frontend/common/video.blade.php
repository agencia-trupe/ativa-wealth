<div class="embed-video">
    @if($tipo == 'youtube')
        <iframe src="http://www.youtube.com/embed/{{ $codigo }}" width="600" height" 400" class="video-player" frameborder="0" allowfullscreen></iframe>
    @elseif($tipo == 'vimeo')
        <iframe src="https://player.vimeo.com/video/{{ $codigo }}" class="video-player" frameborder="0" allowfullscreen></iframe>
    @endif
</div>
