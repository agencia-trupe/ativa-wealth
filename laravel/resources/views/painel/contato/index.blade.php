@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Informações de Contato</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.contato.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.common.flash')

@foreach([
    'saopaulo' => 'São Paulo',
    'riodejaneiro' => 'Rio de Janeiro',
    'curitiba' => 'Curitiba',
    'portoalegre' => 'Porto Alegre',
    'belohorizonte' => 'Belo Horizonte'
] as $key => $value)
    <div class="form-group">
        {!! Form::label($key, $value) !!}
        {!! Form::textarea($key, null, ['class' => 'form-control ckeditor', 'data-editor' => 'endereco']) !!}
    </div>

    <hr>
@endforeach

    <div class="form-group">
        {!! Form::label('email', 'E-mail') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('facebook', 'Facebook (Opcional)') !!}
        {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('twitter', 'Twitter (Opcional)') !!}
        {!! Form::text('twitter', null, ['class' => 'form-control']) !!}
    </div>

    <hr>

    <div class="well form-group">
        {!! Form::label('background', 'Background') !!}
        @if($registro->background)
        <img src="{{ url('assets/img/backgrounds/'.$registro->background) }}" style="display:block; margin-bottom: 10px; width: 400px; max-width: 100%;">
        @endif
        {!! Form::file('background', ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Alterar', ['class' => 'btn btn-success']) !!}

    {!! Form::close() !!}

@endsection
