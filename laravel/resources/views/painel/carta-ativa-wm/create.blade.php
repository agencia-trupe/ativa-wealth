@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Carta Ativa WM /</small> Adicionar Arquivo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.carta-ativa-wm.store', 'files' => true]) !!}

        @include('painel.carta-ativa-wm.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
