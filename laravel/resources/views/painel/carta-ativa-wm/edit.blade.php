@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Carta Ativa WM /</small> Editar Arquivo</h2>
    </legend>

    {!! Form::model($arquivo, [
        'route'  => ['painel.carta-ativa-wm.update', $arquivo->id],
        'method' => 'patch',
        'files'  => true])
    !!}

        @include('painel.carta-ativa-wm.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
