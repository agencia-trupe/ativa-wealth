@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Carta Ativa WM
            <a href="{{ route('painel.carta-ativa-wm.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Arquivo</a>
        </h2>
    </legend>

    <div class="row">
        <div class="col-md-9">
        @if(!count($arquivos))
        <div class="alert alert-warning" role="alert">Nenhum arquivo cadastrado.</div>
        @else
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Título</th>
                    <th>Arquivo</th>
                    <th><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>

            <tbody>
            @foreach ($arquivos as $arquivo)
                <tr class="tr-row" id="{{ $arquivo->id }}">
                    <td>{{ $arquivo->data }}</td>
                    <td>{{ $arquivo->titulo }}</td>
                    <td><a href="{{ url('assets/arquivos/'.$arquivo->arquivo) }}" class="btn btn-info btn-sm" target="_blank">
                        <span class="glyphicon glyphicon-download" style="margin-right:10px;"></span>Download
                    </a></td>
                    <td class="crud-actions">
                        {!! Form::open([
                            'route'  => ['painel.carta-ativa-wm.destroy', $arquivo->id],
                            'method' => 'delete'
                        ]) !!}

                        <div class="btn-group btn-group-sm">
                            <a href="{{ route('painel.carta-ativa-wm.edit', $arquivo->id ) }}" class="btn btn-primary btn-sm pull-left">
                                <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                            </a>

                            <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                        </div>

                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @endif

        {!! $arquivos->render() !!}
        </div>

        <div class="col-md-3">
            <div class="well form-group">
                {!! Form::model($cartaAtiva_background, [
                    'route'  => ['painel.carta-ativa-wm.background', $cartaAtiva_background->id],
                    'method' => 'patch',
                    'files'  => true])
                !!}

                {!! Form::label('background', 'Background') !!}

                @if($cartaAtiva_background->background)
                    <img src="{{ url('assets/img/backgrounds/'.$cartaAtiva_background->background) }}" style="display:block; margin-bottom: 10px; width: 400px; max-width: 100%;">
                @endif

                {!! Form::file('background', ['class' => 'form-control']) !!}
                {!! Form::submit('Alterar', ['class' => 'btn btn-sm btn-success', 'style' => 'margin-top:10px']) !!}

                {!! Form::close() !!}
            </div>

        </div>
    </div>

@endsection
