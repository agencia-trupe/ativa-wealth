@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Imprensa
            <a href="{{ route('painel.imprensa.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Artigo</a>
        </h2>
    </legend>

    <div class="row">
        <div class="col-md-9">
        @if(!count($imprensa))
        <div class="alert alert-warning" role="alert">Nenhum artigo cadastrado.</div>
        @else
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Título</th>
                    <th>Imagens</th>
                    <th><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>

            <tbody>
            @foreach ($imprensa as $artigo)
                <tr class="tr-row" id="{{ $artigo->id }}">
                    <td>{{ $artigo->data }}</td>
                    <td>{{ $artigo->titulo }}</td>
                    <td><a href="{{ route('painel.imprensa.imagens.index', $artigo->id) }}" class="btn btn-info btn-sm">
                        <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                    </a></td>
                    <td class="crud-actions">
                        {!! Form::open([
                            'route'  => ['painel.imprensa.destroy', $artigo->id],
                            'method' => 'delete'
                        ]) !!}

                        <div class="btn-group btn-group-sm">
                            <a href="{{ route('painel.imprensa.edit', $artigo->id ) }}" class="btn btn-primary btn-sm pull-left">
                                <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                            </a>

                            <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                        </div>

                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @endif

        {!! $imprensa->render() !!}
        </div>

        <div class="col-md-3">
            <div class="well form-group">
                {!! Form::model($imprensa_bg, [
                    'route'  => ['painel.imprensa.background', $imprensa_bg->id],
                    'method' => 'patch',
                    'files'  => true])
                !!}

                {!! Form::label('background', 'Background') !!}

                @if($imprensa_bg->background)
                    <img src="{{ url('assets/img/backgrounds/'.$imprensa_bg->background) }}" style="display:block; margin-bottom: 10px; width: 400px; max-width: 100%;">
                @endif

                {!! Form::file('background', ['class' => 'form-control']) !!}
                {!! Form::submit('Alterar', ['class' => 'btn btn-sm btn-success', 'style' => 'margin-top:10px']) !!}

                {!! Form::close() !!}
            </div>

        </div>
    </div>

@endsection
