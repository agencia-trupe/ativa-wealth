@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::text('data', null, ['class' => 'form-control datepicker']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'imprensa']) !!}
</div>

<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label('video_tipo', 'Vídeo - Tipo') !!}
        {!! Form::select('video_tipo', ['youtube' => 'YouTube', 'vimeo' => 'Vimeo'], null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-6">
        {!! Form::label('video_codigo', 'Vídeo - Código (opcional)') !!}
        {!! Form::text('video_codigo', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo (opcional)') !!}
@if($submitText == 'Alterar' && $imprensa->arquivo)
    <a href="{{ url('assets/arquivos/'.$imprensa->arquivo) }}" target="_blank" style="display:block;margin:10px 0;">{{ $imprensa->arquivo }}</a>
@endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.imprensa.index') }}" class="btn btn-default btn-voltar">Voltar</a>
