@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Imprensa /</small> Editar Artigo</h2>
    </legend>

    {!! Form::model($imprensa, [
        'route'  => ['painel.imprensa.update', $imprensa->id],
        'method' => 'patch',
        'files'  => true])
    !!}

        @include('painel.imprensa.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
