@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Imprensa /</small> Adicionar Artigo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.imprensa.store', 'files' => true]) !!}

        @include('painel.imprensa.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
