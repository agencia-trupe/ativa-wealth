@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Políticas do Site e de Negociação
            <a href="{{ route('painel.documentos.index') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-file" style="margin-right:10px;"></span>Documentos</a>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.politicas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.common.flash')

    <div class="form-group">
        {!! Form::label('texto', 'Texto') !!}
        {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'politicas']) !!}
    </div>

    <div class="well form-group">
        {!! Form::label('background', 'Background') !!}
        @if($registro->background)
        <img src="{{ url('assets/img/backgrounds/'.$registro->background) }}" style="display:block; margin-bottom: 10px; width: 400px; max-width: 100%;">
        @endif
        {!! Form::file('background', ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Alterar', ['class' => 'btn btn-success']) !!}

    {!! Form::close() !!}

@endsection