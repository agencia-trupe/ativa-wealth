@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home /</small> Editar Banner</h2>
    </legend>

    {!! Form::model($banner, [
        'route'  => ['painel.home.update', $banner->id],
        'method' => 'patch',
        'files'  => true])
    !!}

        @include('painel.home.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
