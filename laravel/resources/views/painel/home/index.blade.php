@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Home
            @unless($banners->count() >= 6)
            <a href="{{ route('painel.home.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Banner</a>
            @endunless
        </h2>
    </legend>

    <div class="row">
        <div class="col-md-9">
        @if(!count($banners))
        <div class="alert alert-warning" role="alert">Nenhum banner cadastrado.</div>
        @else
        <table class="table table-striped table-bordered table-hover table-sortable" data-table="banners">
            <thead>
                <tr>
                    <th>Ordenar</th>
                    <th>Imagem</th>
                    <th>Título</th>
                    <th><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>

            <tbody>
            @foreach ($banners as $banner)
                <tr class="tr-row" id="{{ $banner->id }}">
                    <td>
                        <a href="#" class="btn btn-info btn-sm btn-move">
                            <span class="glyphicon glyphicon-move"></span>
                        </a>
                    </td>
                    <td><img src="{{ url('assets/img/banners/'.$banner->imagem) }}" style="width:100%;height:auto;max-width:200px"></td>
                    <td>{{ $banner->titulo }}</td>
                    <td class="crud-actions">
                        {!! Form::open([
                            'route'  => ['painel.home.destroy', $banner->id],
                            'method' => 'delete'
                        ]) !!}

                        <div class="btn-group btn-group-sm">
                            <a href="{{ route('painel.home.edit', $banner->id ) }}" class="btn btn-primary btn-sm pull-left">
                                <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                            </a>

                            <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                        </div>

                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @endif
        </div>

        <div class="col-md-3">
            <div class="well form-group">
                {!! Form::model($home_bg, [
                    'route'  => ['painel.home.background', $home_bg->id],
                    'method' => 'patch',
                    'files'  => true])
                !!}

                {!! Form::label('background', 'Background') !!}

                @if($home_bg->background)
                    <img src="{{ url('assets/img/backgrounds/'.$home_bg->background) }}" style="display:block; margin-bottom: 10px; width: 400px; max-width: 100%;">
                @endif

                {!! Form::file('background', ['class' => 'form-control']) !!}
                {!! Form::submit('Alterar', ['class' => 'btn btn-sm btn-success', 'style' => 'margin-top:10px']) !!}

                {!! Form::close() !!}
            </div>

        </div>
    </div>

@endsection
