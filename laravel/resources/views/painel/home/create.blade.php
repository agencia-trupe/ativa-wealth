@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home /</small> Adicionar Banner</h2>
    </legend>

    {!! Form::open(['route' => 'painel.home.store', 'files' => true]) !!}

        @include('painel.home.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
