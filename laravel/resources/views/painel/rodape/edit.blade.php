@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Rodapé</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.rodape.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.rodape.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
