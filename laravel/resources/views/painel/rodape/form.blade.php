@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('endereco', 'Endereço') !!}
    {!! Form::textarea('endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
</div>

<div class="form-group">
    {!! Form::label('disclaimer', 'Disclaimer') !!}
    {!! Form::textarea('disclaimer', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('cor_fundo', 'Cor de Fundo') !!}
            {!! Form::text('cor_fundo', null, ['class' => 'form-control jscolor']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('cor_texto', 'Cor do Texto') !!}
            {!! Form::text('cor_texto', null, ['class' => 'form-control jscolor']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
