@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Redes Sociais /</small> Adicionar Rede Social</h2>
    </legend>

    {!! Form::open(['route' => 'painel.redes-sociais.store', 'files' => true]) !!}

        @include('painel.redes-sociais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
