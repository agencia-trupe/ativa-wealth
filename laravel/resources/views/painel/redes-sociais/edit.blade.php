@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Redes Sociais /</small> Editar Rede Social</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.redes-sociais.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.redes-sociais.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
