@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Ativa Wealth</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.ativa-wealth.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.common.flash')

    <div class="form-group">
        {!! Form::label('texto', 'Texto') !!}
        {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
    </div>

    <div class="row">
        <div class="form-group col-md-6">
            {!! Form::label('video_tipo', 'Vídeo - Tipo') !!}
            {!! Form::select('video_tipo', ['youtube' => 'YouTube', 'vimeo' => 'Vimeo'], null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-md-6">
            {!! Form::label('video_codigo', 'Vídeo - Código (opcional)') !!}
            {!! Form::text('video_codigo', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="well form-group">
        {!! Form::label('background', 'Background') !!}
        @if($registro->background)
        <img src="{{ url('assets/img/backgrounds/'.$registro->background) }}" style="display:block; margin-bottom: 10px; width: 400px; max-width: 100%;">
        @endif
        {!! Form::file('background', ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Alterar', ['class' => 'btn btn-success']) !!}

    {!! Form::close() !!}

@endsection