@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.politicas.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Políticas
    </a>

    <legend>
        <h2>
            Documentos
            <a href="{{ route('painel.documentos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Documento</a>
        </h2>
    </legend>

    <div class="row" style="margin-bottom:20px">
        <div class="form-group col-sm-4">
            {!! Form::select('filtro', $categorias, Input::get('filtro'), ['class' => 'form-control', 'id' => 'filtro-select', 'placeholder' => 'Todas as Categorias', 'data-route' => 'painel/documentos']) !!}
        </div>
        <div class="col-sm-4" style="padding-left:0">
        <a href="{{ route('painel.documentos.categorias.index') }}" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span><small>Editar Categorias</small></a>
        </div>
        @if(!$filtro)
        <div class="col-sm-4">
            <p class="alert alert-info small" style="margin-bottom: 15px; height:45px; padding: 12px 15px;">
                <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
                Selecione uma categoria para ordenar os documentos.
            </p>
        </div>
        @endif
    </div>

    @if(!count($documentos))
    <div class="alert alert-warning" role="alert">Nenhum documento cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="documentos">
        <thead>
            <tr>
                @if($filtro)<th>Ordenar</th>@endif
                @if(!$filtro)<th>Categoria</th>@endif
                <th>Título</th>
                <th>Arquivo</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($documentos as $documento)
            <tr class="tr-row" id="{{ $documento->id }}">
                @if($filtro)
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                @endif
                @if(!$filtro)
                <td>
                    @if($documento->categoria){{ $documento->categoria->titulo }}@endif
                </td>
                @endif
                <td>{{ $documento->titulo }}</td>
                <td>
                    <a href="{{ url('assets/documentos/'.$documento->arquivo) }}" target="_blank" class="btn btn-sm btn-info">
                        <span class="glyphicon glyphicon-download" style="margin-right:5px;"></span>
                        Download
                    </a>
                </td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.documentos.destroy', $documento->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.documentos.edit', $documento->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
