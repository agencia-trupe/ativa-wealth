@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('documentos_categoria_id', 'Categoria') !!}
    {!! Form::select('documentos_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if($submitText == 'Alterar' && $documento->arquivo)
        <a href="{{ url('assets/documentos/'.$documento->arquivo) }}" target="_blank" style="display:block;margin:10px 0;">{{ $documento->arquivo }}</a>
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.documentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
