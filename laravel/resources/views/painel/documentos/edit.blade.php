@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Documentos /</small> Editar Documento</h2>
    </legend>

    {!! Form::model($documento, [
        'route'  => ['painel.documentos.update', $documento->id],
        'method' => 'patch',
        'files'  => true])
    !!}

        @include('painel.documentos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
