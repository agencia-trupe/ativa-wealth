@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Documentos /</small> Adicionar Documento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.documentos.store', 'files' => true]) !!}

        @include('painel.documentos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
