@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Documentos /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route' => ['painel.documentos.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

        @include('painel.documentos.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
