@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Documentos /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.documentos.categorias.store']) !!}

        @include('painel.documentos.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
