@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Filosofia</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.filosofia.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.common.flash')

    <div class="row">
        <div class="form-group col-md-4">
            {!! Form::label('texto_abertura_1', 'Abertura - Texto 1') !!}
            {!! Form::textarea('texto_abertura_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>

        <div class="form-group col-md-4">
            {!! Form::label('texto_abertura_2', 'Abertura - Texto 2') !!}
            {!! Form::textarea('texto_abertura_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>

        <div class="col-md-4">
            <div class="well form-group">
                {!! Form::label('background_abertura', 'Abertura - Background') !!}
                @if($registro->background_abertura)
                <img src="{{ url('assets/img/backgrounds/'.$registro->background_abertura) }}" style="display:block; margin-bottom: 10px; width: 400px; max-width: 100%;">
                @endif
                {!! Form::file('background_abertura', ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>

@foreach(range(1,4) as $key)
    <hr>

    <div class="row">
        <div class="form-group col-md-4">
            {!! Form::label('titulo_'.$key, $key.' - Título') !!}
            {!! Form::textarea('titulo_'.$key, null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>

        <div class="form-group col-md-4">
            {!! Form::label('texto_'.$key, $key.' - Texto') !!}
            {!! Form::textarea('texto_'.$key, null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>

        <div class="col-md-4">
            <div class="well form-group">
                {!! Form::label('background_'.$key, $key.' - Background') !!}
                @if($registro->{'background_'.$key})
                <img src="{{ url('assets/img/backgrounds/'.$registro->{'background_'.$key}) }}" style="display:block; margin-bottom: 10px; width: 400px; max-width: 100%;">
                @endif
                {!! Form::file('background_'.$key, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
@endforeach

    {!! Form::submit('Alterar', ['class' => 'btn btn-success']) !!}

    {!! Form::close() !!}

@endsection