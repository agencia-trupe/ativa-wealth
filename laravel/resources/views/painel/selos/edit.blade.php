@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Selos /</small> Editar Selo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.selos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.selos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
