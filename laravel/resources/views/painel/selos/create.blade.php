@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Selos /</small> Adicionar Selo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.selos.store', 'files' => true]) !!}

        @include('painel.selos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
