<ul class="nav navbar-nav">
    <li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.home.index') }}">Home</a>
    </li>

    <li @if(str_is('painel.somos-ativa*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.somos-ativa.index') }}">Somos Ativa</a>
    </li>

    <li @if(str_is('painel.filosofia*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.filosofia.index') }}">Filosofia</a>
    </li>

    <li @if(str_is('painel.ativa-wealth*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.ativa-wealth.index') }}">Ativa Wealth</a>
    </li>

    <li @if(str_is('painel.imprensa*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.imprensa.index') }}">Imprensa</a>
    </li>

    <li @if(str_is('painel.carta-ativa-wm*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.carta-ativa-wm.index') }}">Carta Ativa WM</a>
    </li>

    <li @if(str_is('painel.politicas*', Route::currentRouteName()) || str_is('painel.documentos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.politicas.index') }}">Políticas</a>
    </li>

    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
