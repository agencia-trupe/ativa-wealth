<?php

use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('home_bg')->insert(['background' => '']);
        DB::table('somos_ativa')->insert(['background' => '']);
        DB::table('filosofia')->insert(['background_abertura' => '']);
        DB::table('ativa_wealth')->insert(['background' => '']);
        DB::table('imprensa_bg')->insert(['background' => '']);
        DB::table('carta_ativa_bg')->insert(['background' => '']);
        DB::table('politicas')->insert(['background' => '']);
        DB::table('contato')->insert(['background' => '']);
    }
}
