<?php

use Illuminate\Database\Seeder;

class RodapeSeeder extends Seeder
{
    public function run()
    {
        DB::table('rodape')->insert([
            'endereco' => '',
            'disclaimer' => '',
            'cor_fundo' => '',
            'cor_texto' => '',
        ]);
    }
}
