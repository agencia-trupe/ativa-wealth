<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilosofiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filosofia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('background_abertura')->nullable();
            $table->text('texto_abertura_1')->nullable();
            $table->text('texto_abertura_2')->nullable();
            $table->string('background_1')->nullable();
            $table->text('titulo_1')->nullable();
            $table->text('texto_1')->nullable();
            $table->string('background_2')->nullable();
            $table->text('titulo_2')->nullable();
            $table->text('texto_2')->nullable();
            $table->string('background_3')->nullable();
            $table->text('titulo_3')->nullable();
            $table->text('texto_3')->nullable();
            $table->string('background_4')->nullable();
            $table->text('titulo_4')->nullable();
            $table->text('texto_4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('filosofia');
    }
}
