<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImprensaImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imprensa_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('imprensa_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem')->nullable();
            $table->timestamps();

            $table->foreign('imprensa_id')->references('id')->on('imprensa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('imprensa_imagens');
    }
}
