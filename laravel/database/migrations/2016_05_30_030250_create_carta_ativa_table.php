<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartaAtivaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carta_ativa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data');
            $table->text('titulo');
            $table->string('arquivo');
            $table->timestamps();
        });

        Schema::create('carta_ativa_bg', function (Blueprint $table) {
            $table->increments('id');
            $table->string('background')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('carta_ativa');
        Schema::drop('carta_ativa_bg');
    }
}
