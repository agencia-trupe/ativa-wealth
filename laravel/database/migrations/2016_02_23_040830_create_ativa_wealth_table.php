<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtivaWealthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ativa_wealth', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto')->nullable();
            $table->string('video_tipo')->nullable();
            $table->string('video_codigo')->nullable();
            $table->string('background')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ativa_wealth');
    }
}
