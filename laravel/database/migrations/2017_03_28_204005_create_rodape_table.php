<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRodapeTable extends Migration
{
    public function up()
    {
        Schema::create('rodape', function (Blueprint $table) {
            $table->increments('id');
            $table->text('endereco');
            $table->text('disclaimer');
            $table->string('cor_fundo');
            $table->string('cor_texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('rodape');
    }
}
