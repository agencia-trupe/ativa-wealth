<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImprensaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imprensa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data')->nullable();
            $table->string('titulo')->nullable();
            $table->string('slug')->nullable();
            $table->text('texto')->nullable();
            $table->string('arquivo')->nullable();
            $table->string('video_tipo')->nullable();
            $table->string('video_codigo')->nullable();
            $table->timestamps();
        });

        Schema::create('imprensa_bg', function (Blueprint $table) {
            $table->increments('id');
            $table->string('background')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('imprensa');
        Schema::drop('imprensa_bg');
    }
}
