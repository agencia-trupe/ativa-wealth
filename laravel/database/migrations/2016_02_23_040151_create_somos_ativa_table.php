<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSomosAtivaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('somos_ativa', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto')->nullable();
            $table->string('video_tipo')->nullable();
            $table->string('video_codigo')->nullable();
            $table->string('background')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('somos_ativa');
    }
}
