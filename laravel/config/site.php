<?php

return [

    'name'        => 'Ativa Wealth Management',
    'title'       => 'Ativa Wealth Management',
    'description' => 'A ATIVA Wealth Management é uma gestora e patrimônios que trabalha com produtos exclusivos de acordo com o seu perfil de investidor. Buscamos os melhores investimentos visando à relação entre risco e retorno ao longo dos anos.',
    'keywords'    => 'ATIVAWM, ATIVA WM, Gestão de Patrimônios, Wealth Management, ATIVA Wealth Management, ATIVA Investimentos',
    'share_image' => 'ativa-compart-facebook.jpg',
    'analytics'   => 'UA-78860100-1'

];
