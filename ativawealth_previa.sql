-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: 31-Mar-2016 às 22:40
-- Versão do servidor: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ativawealth`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ativa_wealth`
--

CREATE TABLE `ativa_wealth` (
`id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `background` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `ativa_wealth`
--

INSERT INTO `ativa_wealth` (`id`, `texto`, `video_tipo`, `video_codigo`, `background`, `created_at`, `updated_at`) VALUES
(1, '<p>WEALTH MANAGEMENT</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae fringilla enim. Fusce ornare, tellus in feugiat eleifend, ipsum erat lobortis urna, vel finibus quam tellus et libero. Ut suscipit accumsan maximus. Donec nec malesuada tellus. Nulla ut ornare metus. Ut consectetur diam elit, sit amet tristique quam ornare sed. Donec ac nunc eu nisl tempus consectetur sit amet ut purus. Mauris non imperdiet turpis, et auctor velit.</p>\r\n\r\n<p>Donec egestas, lacus ac ornare fringilla, nulla odio hendrerit mauris, eu vehicula eros eros nec dui. Suspendisse potenti. Vivamus pulvinar velit mollis ex mattis hendrerit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque ut tristique dui. Integer scelerisque vehicula urna at interdum. Cras ac luctus felis. Nulla mauris eros, posuere malesuada porta vitae, porttitor non est. Sed vestibulum purus eu nulla imperdiet pulvinar. Etiam sollicitudin porta eros nec volutpat. Praesent tempor ullamcorper sodales.</p>\r\n', 'youtube', 'BsogwgZJW_Q', 'bg5_20160225151326.jpg', '0000-00-00 00:00:00', '2016-02-25 18:21:42');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
`id` int(10) unsigned NOT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `saopaulo_endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `saopaulo_cep` text COLLATE utf8_unicode_ci NOT NULL,
  `saopaulo_telefone` text COLLATE utf8_unicode_ci NOT NULL,
  `saopaulo_fax` text COLLATE utf8_unicode_ci NOT NULL,
  `riodejaneiro_endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `riodejaneiro_cep` text COLLATE utf8_unicode_ci NOT NULL,
  `riodejaneiro_telefone` text COLLATE utf8_unicode_ci NOT NULL,
  `riodejaneiro_fax` text COLLATE utf8_unicode_ci NOT NULL,
  `curitiba_endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `curitiba_cep` text COLLATE utf8_unicode_ci NOT NULL,
  `curitiba_telefone` text COLLATE utf8_unicode_ci NOT NULL,
  `curitiba_fax` text COLLATE utf8_unicode_ci NOT NULL,
  `portoalegre_endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `portoalegre_cep` text COLLATE utf8_unicode_ci NOT NULL,
  `portoalegre_telefone` text COLLATE utf8_unicode_ci NOT NULL,
  `portoalegre_fax` text COLLATE utf8_unicode_ci NOT NULL,
  `belohorizonte_endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `belohorizonte_cep` text COLLATE utf8_unicode_ci NOT NULL,
  `belohorizonte_telefone` text COLLATE utf8_unicode_ci NOT NULL,
  `belohorizonte_fax` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `background`, `saopaulo_endereco`, `saopaulo_cep`, `saopaulo_telefone`, `saopaulo_fax`, `riodejaneiro_endereco`, `riodejaneiro_cep`, `riodejaneiro_telefone`, `riodejaneiro_fax`, `curitiba_endereco`, `curitiba_cep`, `curitiba_telefone`, `curitiba_fax`, `portoalegre_endereco`, `portoalegre_cep`, `portoalegre_telefone`, `portoalegre_fax`, `belohorizonte_endereco`, `belohorizonte_cep`, `belohorizonte_telefone`, `belohorizonte_fax`, `facebook`, `twitter`, `email`, `created_at`, `updated_at`) VALUES
(1, 'bg4_20160225152031.jpg', '<p>Rua Iguatemi, 192,</p>\r\n\r\n<p>15&ordm; e 16&ordm; andares</p>\r\n\r\n<p>Itaim Bibi</p>\r\n\r\n<p>S&atilde;o Paulo/SP</p>\r\n', '01451-011', '(11) 4950-7200 / (11) 4097-7200', '(11) 4950-7250 / (11) 4097-7250', '<p>Av. das Am&eacute;ricas, 3500,</p>\r\n\r\n<p>Sls 314 318 - Le Monde</p>\r\n\r\n<p>Ed. Londres</p>\r\n\r\n<p>Barra da Tijuca</p>\r\n\r\n<p>Rio de Janeiro/RJ</p>\r\n', '22640-102', '(21) 3515-0200 / (21) 3176-8321', '(21) 3176-8321', '<p>Al. Dr. Carlos de Carvalho, 417,</p>\r\n\r\n<p>30&ordm;&nbsp;andar, Sala 300</p>\r\n\r\n<p>Centro</p>\r\n\r\n<p>Curitiba/PR</p>\r\n', '80410-180', '(41) 3075-7400', '(41) 3075-7406', '<p>Av. Nilo Pe&ccedil;anha, 2825,</p>\r\n\r\n<p>Ed. Iguatemi Corporate,</p>\r\n\r\n<p>Conjunto 1503</p>\r\n\r\n<p>Tr&ecirc;s Figueiras</p>\r\n\r\n<p>Porto Alegre/RS</p>\r\n', '91330-001', '(51) 3017-8707', '', '<p>Rua dos Amor&eacute;s, 2001,</p>\r\n\r\n<p>Sls 404 / 406</p>\r\n\r\n<p>Lourdes</p>\r\n\r\n<p>Belo Horizonte/MG</p>\r\n', '30140-072', '(31) 3025-0601', '', '/', '/', 'contato@ativawealth.com.br', '0000-00-00 00:00:00', '2016-02-25 18:24:54');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
`id` int(10) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `documentos`
--

CREATE TABLE `documentos` (
`id` int(10) unsigned NOT NULL,
  `documentos_categoria_id` int(10) unsigned DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `documentos_categorias`
--

CREATE TABLE `documentos_categorias` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `filosofia`
--

CREATE TABLE `filosofia` (
`id` int(10) unsigned NOT NULL,
  `background_abertura` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `texto_abertura_1` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_abertura_2` text COLLATE utf8_unicode_ci NOT NULL,
  `background_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `titulo_1` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_1` text COLLATE utf8_unicode_ci NOT NULL,
  `background_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `titulo_2` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2` text COLLATE utf8_unicode_ci NOT NULL,
  `background_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `titulo_3` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_3` text COLLATE utf8_unicode_ci NOT NULL,
  `background_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `titulo_4` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_4` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `filosofia`
--

INSERT INTO `filosofia` (`id`, `background_abertura`, `texto_abertura_1`, `texto_abertura_2`, `background_1`, `titulo_1`, `texto_1`, `background_2`, `titulo_2`, `texto_2`, `background_3`, `titulo_3`, `texto_3`, `background_4`, `titulo_4`, `texto_4`, `created_at`, `updated_at`) VALUES
(1, 'bg8_20160225152828.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras semper lacus et eros eleifend, at convallis augue imperdiet.</p>\r\n', '<p>Quisque laoreet tempor nibh a blandit. Aliquam pharetra aliquet nunc, vel gravida nulla porta ac.</p>\r\n', 'bg5_20160225152828.jpg', '<p>Solu&ccedil;&otilde;es Financeiras</p>\r\n\r\n<p><strong>Personalizadas</strong></p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras semper lacus et eros eleifend, at convallis augue imperdiet. Quisque laoreet tempor nibh a blandit. Aliquam pharetra aliquet nunc, vel gravida nulla porta ac. Sed vitae vehicula nisi. In dapibus velit ac pulvinar auctor. Mauris eu dignissim libero, sit amet rhoncus sem. Etiam eget neque condimentum, auctor metus in, tempus nisi. Nulla tristique tortor nec ante vulputate feugiat. Aliquam nec pulvinar sapien.</p>\r\n\r\n<p>Aliquam placerat mauris nec ornare pellentesque. Pellentesque ac imperdiet felis. Quisque porttitor nibh ornare, ullamcorper lorem id, interdum neque. Donec tristique tristique bibendum. Aenean quis tristique tellus, vel malesuada augue. Maecenas mattis non lacus vitae auctor. Vivamus vitae nibh commodo ligula vehicula molestie. Donec lacus sem, eleifend eget tempus suscipit, eleifend vitae dolor. Duis imperdiet mi rutrum lacus ultrices, quis pulvinar felis vulputate. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus id ullamcorper lacus. Vestibulum at erat eget arcu tincidunt facilisis quis et enim. Phasellus semper urna pharetra blandit lacinia.</p>\r\n', 'bg2_20160225152829.jpg', '<p>Investimento de Capital</p>\r\n\r\n<p>com <strong>Seguran&ccedil;a</strong></p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras semper lacus et eros eleifend, at convallis augue imperdiet. Quisque laoreet tempor nibh a blandit. Aliquam pharetra aliquet nunc, vel gravida nulla porta ac. Sed vitae vehicula nisi. In dapibus velit ac pulvinar auctor. Mauris eu dignissim libero, sit amet rhoncus sem. Etiam eget neque condimentum, auctor metus in, tempus nisi. Nulla tristique tortor nec ante vulputate feugiat. Aliquam nec pulvinar sapien.</p>\r\n\r\n<p>Aliquam placerat mauris nec ornare pellentesque. Pellentesque ac imperdiet felis. Quisque porttitor nibh ornare, ullamcorper lorem id, interdum neque. Donec tristique tristique bibendum. Aenean quis tristique tellus, vel malesuada augue. Maecenas mattis non lacus vitae auctor. Vivamus vitae nibh commodo ligula vehicula molestie. Donec lacus sem, eleifend eget tempus suscipit, eleifend vitae dolor. Duis imperdiet mi rutrum lacus ultrices, quis pulvinar felis vulputate. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus id ullamcorper lacus. Vestibulum at erat eget arcu tincidunt facilisis quis et enim. Phasellus semper urna pharetra blandit lacinia.</p>\r\n', 'bg7_20160225152830.jpg', '<p>Reputa&ccedil;&atilde;o e <strong>Credibilidade</strong></p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras semper lacus et eros eleifend, at convallis augue imperdiet. Quisque laoreet tempor nibh a blandit. Aliquam pharetra aliquet nunc, vel gravida nulla porta ac. Sed vitae vehicula nisi. In dapibus velit ac pulvinar auctor. Mauris eu dignissim libero, sit amet rhoncus sem. Etiam eget neque condimentum, auctor metus in, tempus nisi. Nulla tristique tortor nec ante vulputate feugiat. Aliquam nec pulvinar sapien.</p>\r\n\r\n<p>Aliquam placerat mauris nec ornare pellentesque. Pellentesque ac imperdiet felis. Quisque porttitor nibh ornare, ullamcorper lorem id, interdum neque. Donec tristique tristique bibendum. Aenean quis tristique tellus, vel malesuada augue. Maecenas mattis non lacus vitae auctor. Vivamus vitae nibh commodo ligula vehicula molestie. Donec lacus sem, eleifend eget tempus suscipit, eleifend vitae dolor. Duis imperdiet mi rutrum lacus ultrices, quis pulvinar felis vulputate. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus id ullamcorper lacus. Vestibulum at erat eget arcu tincidunt facilisis quis et enim. Phasellus semper urna pharetra blandit lacinia.</p>\r\n', 'bg6_20160225152830.jpg', '<p>Atendimento Qualificado</p>\r\n\r\n<p>e <strong>Comprometido</strong></p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras semper lacus et eros eleifend, at convallis augue imperdiet. Quisque laoreet tempor nibh a blandit. Aliquam pharetra aliquet nunc, vel gravida nulla porta ac. Sed vitae vehicula nisi. In dapibus velit ac pulvinar auctor. Mauris eu dignissim libero, sit amet rhoncus sem. Etiam eget neque condimentum, auctor metus in, tempus nisi. Nulla tristique tortor nec ante vulputate feugiat. Aliquam nec pulvinar sapien.</p>\r\n\r\n<p>Aliquam placerat mauris nec ornare pellentesque. Pellentesque ac imperdiet felis. Quisque porttitor nibh ornare, ullamcorper lorem id, interdum neque. Donec tristique tristique bibendum. Aenean quis tristique tellus, vel malesuada augue. Maecenas mattis non lacus vitae auctor. Vivamus vitae nibh commodo ligula vehicula molestie. Donec lacus sem, eleifend eget tempus suscipit, eleifend vitae dolor. Duis imperdiet mi rutrum lacus ultrices, quis pulvinar felis vulputate. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus id ullamcorper lacus. Vestibulum at erat eget arcu tincidunt facilisis quis et enim. Phasellus semper urna pharetra blandit lacinia.</p>\r\n', '0000-00-00 00:00:00', '2016-02-26 17:17:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `home`
--

CREATE TABLE `home` (
`id` int(10) unsigned NOT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `home`
--

INSERT INTO `home` (`id`, `background`, `video_tipo`, `video_codigo`, `created_at`, `updated_at`) VALUES
(1, 'bg1_20160225150832.jpg', 'youtube', 'BsogwgZJW_Q', '0000-00-00 00:00:00', '2016-02-25 18:08:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imprensa`
--

CREATE TABLE `imprensa` (
`id` int(10) unsigned NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `imprensa`
--

INSERT INTO `imprensa` (`id`, `data`, `titulo`, `slug`, `texto`, `arquivo`, `video_tipo`, `video_codigo`, `created_at`, `updated_at`) VALUES
(1, '2016-02-25', 'Artigo de Exemplo', 'artigo-de-exemplo', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras semper lacus et eros eleifend, at convallis augue imperdiet. Quisque laoreet tempor nibh a blandit. Aliquam pharetra aliquet nunc, vel gravida nulla porta ac. Sed vitae vehicula nisi. In dapibus velit ac pulvinar auctor. Mauris eu dignissim libero, sit amet rhoncus sem. Etiam eget neque condimentum, auctor metus in, tempus nisi. Nulla tristique tortor nec ante vulputate feugiat. Aliquam nec pulvinar sapien.</p>\r\n\r\n<p><a href="#">Link</a></p>\r\n\r\n<p>Aliquam placerat mauris nec ornare pellentesque. Pellentesque ac imperdiet felis. Quisque porttitor nibh ornare, ullamcorper lorem id, interdum neque. Donec tristique tristique bibendum. Aenean quis tristique tellus, vel malesuada augue. Maecenas mattis non lacus vitae auctor. Vivamus vitae nibh commodo ligula vehicula molestie. Donec lacus sem, eleifend eget tempus suscipit, eleifend vitae dolor. Duis imperdiet mi rutrum lacus ultrices, quis pulvinar felis vulputate. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus id ullamcorper lacus. Vestibulum at erat eget arcu tincidunt facilisis quis et enim. Phasellus semper urna pharetra blandit lacinia.</p>\r\n', '20160225153004_exemplo.pdf', 'youtube', 'BsogwgZJW_Q', '2016-02-25 18:30:04', '2016-02-25 18:30:04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imprensa_bg`
--

CREATE TABLE `imprensa_bg` (
`id` int(10) unsigned NOT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `imprensa_bg`
--

INSERT INTO `imprensa_bg` (`id`, `background`, `created_at`, `updated_at`) VALUES
(1, 'bg3_20160225150602.jpg', '0000-00-00 00:00:00', '2016-02-25 18:06:02');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imprensa_imagens`
--

CREATE TABLE `imprensa_imagens` (
`id` int(10) unsigned NOT NULL,
  `imprensa_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `imprensa_imagens`
--

INSERT INTO `imprensa_imagens` (`id`, `imprensa_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'bg1_20160225153135.jpg', '2016-02-25 18:31:36', '2016-02-25 18:31:36'),
(2, 1, 0, 'bg2_20160225153135.jpg', '2016-02-25 18:31:36', '2016-02-25 18:31:36');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_02_23_040032_create_home_table', 1),
('2016_02_23_040151_create_somos_ativa_table', 1),
('2016_02_23_040413_create_filosofia_table', 1),
('2016_02_23_040830_create_ativa_wealth_table', 1),
('2016_02_23_040928_create_contato_table', 1),
('2016_02_23_144048_create_contatos_recebidos', 1),
('2016_02_25_124703_create_imprensa_table', 1),
('2016_02_25_124713_create_imprensa_imagens_table', 1),
('2016_03_31_175755_create_politicas_table', 1),
('2016_03_31_183109_create_documentos_tables', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_02_23_040032_create_home_table', 1),
('2016_02_23_040151_create_somos_ativa_table', 1),
('2016_02_23_040413_create_filosofia_table', 1),
('2016_02_23_040830_create_ativa_wealth_table', 1),
('2016_02_23_040928_create_contato_table', 1),
('2016_02_23_144048_create_contatos_recebidos', 1),
('2016_02_25_124703_create_imprensa_table', 1),
('2016_02_25_124713_create_imprensa_imagens_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `politicas`
--

CREATE TABLE `politicas` (
`id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `politicas`
--

INSERT INTO `politicas` (`id`, `texto`, `background`, `created_at`, `updated_at`) VALUES
(1, '<h2 class="politica-titulo">POL&Iacute;TICAS DO SITE E DE NEGOCIA&Ccedil;&Atilde;O</h2>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tincidunt ut arcu vel venenatis. Suspendisse posuere orci nec lectus posuere rhoncus. Aenean venenatis turpis eu cursus imperdiet. Nulla vel lobortis libero, et tristique justo. Cras id ante ac libero commodo rutrum quis a ex. Nunc sed pulvinar leo. Fusce consectetur, libero nec luctus pretium, leo nulla feugiat sapien, maximus gravida mi lectus in velit.</p>\r\n\r\n<p>Ut ac libero quis est luctus tempus. Fusce tincidunt erat a aliquet vehicula. Nam tincidunt augue purus, ut imperdiet magna fermentum non. Phasellus dignissim finibus velit sit amet lacinia. Aenean tincidunt vestibulum velit ut fermentum. Quisque malesuada sem lorem, in eleifend est condimentum bibendum. Sed et rhoncus magna.</p>\r\n\r\n<h2 class="politica-titulo">POL&Iacute;TICA DE PRIVACIDADE E SEGURAN&Ccedil;A</h2>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tincidunt ut arcu vel venenatis. Suspendisse posuere orci nec lectus posuere rhoncus. Aenean venenatis turpis eu cursus imperdiet. Nulla vel lobortis libero, et tristique justo. Cras id ante ac libero commodo rutrum quis a ex. Nunc sed pulvinar leo. Fusce consectetur, libero nec luctus pretium, leo nulla feugiat sapien, maximus gravida mi lectus in velit.</p>\r\n\r\n<p>Ut ac libero quis est luctus tempus. Fusce tincidunt erat a aliquet vehicula. Nam tincidunt augue purus, ut imperdiet magna fermentum non. Phasellus dignissim finibus velit sit amet lacinia. Aenean tincidunt vestibulum velit ut fermentum. Quisque malesuada sem lorem, in eleifend est condimentum bibendum. Sed et rhoncus magna.</p>\r\n', 'bg9_20160331204023.jpg', '0000-00-00 00:00:00', '2016-03-31 23:40:25');

-- --------------------------------------------------------

--
-- Estrutura da tabela `somos_ativa`
--

CREATE TABLE `somos_ativa` (
`id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `background` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `somos_ativa`
--

INSERT INTO `somos_ativa` (`id`, `texto`, `video_tipo`, `video_codigo`, `background`, `created_at`, `updated_at`) VALUES
(1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae fringilla enim. Fusce ornare, tellus in feugiat eleifend, ipsum erat lobortis urna, vel finibus quam tellus et libero. Ut suscipit accumsan maximus. Donec nec malesuada tellus. Nulla ut ornare metus. Ut consectetur diam elit, sit amet tristique quam ornare sed. Donec ac nunc eu nisl tempus consectetur sit amet ut purus. Mauris non imperdiet turpis, et auctor velit.</p>\r\n\r\n<p>Donec egestas, lacus ac ornare fringilla, nulla odio hendrerit mauris, eu vehicula eros eros nec dui. Suspendisse potenti. Vivamus pulvinar velit mollis ex mattis hendrerit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque ut tristique dui. Integer scelerisque vehicula urna at interdum. Cras ac luctus felis. Nulla mauris eros, posuere malesuada porta vitae, porttitor non est. Sed vestibulum purus eu nulla imperdiet pulvinar. Etiam sollicitudin porta eros nec volutpat. Praesent tempor ullamcorper sodales.</p>\r\n', 'youtube', 'BsogwgZJW_Q', 'bg6_20160225151250.jpg', '0000-00-00 00:00:00', '2016-02-25 18:12:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$uHCCDYRlSMp4IaEu6CGbCup46AE4N.iGBnYPHK09u/zZ./wg6NlbG', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ativa_wealth`
--
ALTER TABLE `ativa_wealth`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documentos`
--
ALTER TABLE `documentos`
 ADD PRIMARY KEY (`id`), ADD KEY `documentos_documentos_categoria_id_foreign` (`documentos_categoria_id`);

--
-- Indexes for table `documentos_categorias`
--
ALTER TABLE `documentos_categorias`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filosofia`
--
ALTER TABLE `filosofia`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imprensa`
--
ALTER TABLE `imprensa`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imprensa_bg`
--
ALTER TABLE `imprensa_bg`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imprensa_imagens`
--
ALTER TABLE `imprensa_imagens`
 ADD PRIMARY KEY (`id`), ADD KEY `imprensa_imagens_imprensa_id_foreign` (`imprensa_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `politicas`
--
ALTER TABLE `politicas`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `somos_ativa`
--
ALTER TABLE `somos_ativa`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ativa_wealth`
--
ALTER TABLE `ativa_wealth`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `documentos`
--
ALTER TABLE `documentos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `documentos_categorias`
--
ALTER TABLE `documentos_categorias`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `filosofia`
--
ALTER TABLE `filosofia`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `home`
--
ALTER TABLE `home`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `imprensa`
--
ALTER TABLE `imprensa`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `imprensa_bg`
--
ALTER TABLE `imprensa_bg`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `imprensa_imagens`
--
ALTER TABLE `imprensa_imagens`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `politicas`
--
ALTER TABLE `politicas`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `somos_ativa`
--
ALTER TABLE `somos_ativa`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `documentos`
--
ALTER TABLE `documentos`
ADD CONSTRAINT `documentos_documentos_categoria_id_foreign` FOREIGN KEY (`documentos_categoria_id`) REFERENCES `documentos_categorias` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `imprensa_imagens`
--
ALTER TABLE `imprensa_imagens`
ADD CONSTRAINT `imprensa_imagens_imprensa_id_foreign` FOREIGN KEY (`imprensa_id`) REFERENCES `imprensa` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
