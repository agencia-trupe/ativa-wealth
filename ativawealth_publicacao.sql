-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: mysql120.trupe.net
-- Generation Time: 07-Jun-2016 às 16:08
-- Versão do servidor: 5.6.21-69.0-log
-- PHP Version: 5.6.20-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trupe1110`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ativa_wealth`
--

CREATE TABLE `ativa_wealth` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `ativa_wealth`
--

INSERT INTO `ativa_wealth` (`id`, `texto`, `video_tipo`, `video_codigo`, `background`, `created_at`, `updated_at`) VALUES
(1, '<p>Trabalhamos de forma independente com produtos exclusivos e de acordo com o perfil do investidor.</p>\r\n\r\n<p>Atrav&eacute;s de uma an&aacute;lise individualizada de cada um de nossos clientes, buscamos o melhor perfil de investimento visando a melhor rela&ccedil;&atilde;o entre risco e retorno e a preserva&ccedil;&atilde;o de capital ao longo dos anos.</p>\r\n\r\n<p>Levando em considera&ccedil;&atilde;o seus objetivos e necessidades, a nossa gest&atilde;o de patrim&ocirc;nio atua focada em voc&ecirc;.</p>\r\n\r\n<p>S&atilde;o solu&ccedil;&otilde;es e gest&atilde;o para os seus investimentos, com assessoria exclusiva para sua empresa e um &oacute;timo planejamento para o seu futuro.</p>\r\n\r\n<p>Oferecemos um conjunto completo de compet&ecirc;ncias para administrar os seus investimentos de forma personalizada.</p>\r\n\r\n<p>Com produtos diversificados e uma gest&atilde;o eficiente, seu patrim&ocirc;nio ser&aacute; preservado e voc&ecirc; poder&aacute; alcan&ccedil;ar ganhos cada vez maiores.</p>\r\n\r\n<p>Temos uma s&oacute;lida experi&ecirc;ncia na gest&atilde;o e no aconselhamento em diferentes mercados.</p>\r\n', 'youtube', 'BsogwgZJW_Q', 'shutterstock_191283356_20160601143351.jpg', '0000-00-00 00:00:00', '2016-06-01 17:33:54');

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` text COLLATE utf8_unicode_ci,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `titulo`, `descricao`, `link`, `created_at`, `updated_at`) VALUES
(1, 3, 'shutterstock_73471654_20160601152336.jpg', 'ATIVA Wealth Management', 'Enxergar o mercado com os olhos da experiência faz parte da nossa marca.', '#', '2016-05-31 17:00:30', '2016-06-01 18:23:38'),
(2, 0, 'shutterstock_207721282_20160601152035.jpg', 'ATIVA Wealth Management', 'Não importa qual tipo de investidor você é: nós temos a solução que você procura.', '#', '2016-05-31 17:01:05', '2016-06-01 18:20:39'),
(3, 1, 'shutterstock_205052329_20160601183343.jpg', 'ATIVA Wealth Management', 'Temos soluções personalizadas para você.', '#', '2016-06-01 21:33:49', '2016-06-03 16:59:12'),
(4, 2, 'shutterstock_136081736_20160601183638.jpg', 'ATIVA Wealth Management', 'Olhamos de perto as conquistas das pessoas.', '#', '2016-06-01 21:36:41', '2016-06-01 21:36:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `carta_ativa`
--

CREATE TABLE `carta_ativa` (
  `id` int(10) UNSIGNED NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `carta_ativa`
--

INSERT INTO `carta_ativa` (`id`, `data`, `titulo`, `arquivo`, `created_at`, `updated_at`) VALUES
(2, '2016-04', 'Ainda Sobre o Crédito', '20160601143717_04 - Abril - ATIVA Investimentos.pdf', '2016-06-01 17:37:17', '2016-06-01 17:37:17'),
(3, '2016-03', 'Temer', '20160601143856_03 - Maro - ATIVA Investimentos.pdf', '2016-06-01 17:38:56', '2016-06-01 17:38:56'),
(4, '2015-12', 'Retrospectiva', '20160601144015_ATIVA Investimentos - Dezembro.pdf', '2016-06-01 17:40:16', '2016-06-01 17:40:16'),
(5, '2015-10', 'Crédito Privado Incentivado?', '20160601144206_ATIVA Investimentos - outubro v2.pdf', '2016-06-01 17:42:06', '2016-06-01 17:42:06'),
(6, '2015-09', 'Até Onde Vai o Dólar?', '20160601144305_ATIVA Investimentos - setembro v1.pdf', '2016-06-01 17:43:05', '2016-06-01 17:43:05'),
(7, '2015-08', 'Oportunidades', '20160601144340_ATIVA Investimentos - agosto - v4.pdf', '2016-06-01 17:43:40', '2016-06-01 17:43:40'),
(8, '2015-07', 'Cada Vez Mais Renda Fixa', '20160601144417_ATIVA Investimentos - Julho - v1.pdf', '2016-06-01 17:44:17', '2016-06-01 17:44:17'),
(9, '2015-06', 'Nota Estruturada à Brasileira', '20160601144501_ATIVA Investimentos - Junho- verso01.pdf', '2016-06-01 17:45:01', '2016-06-01 17:45:01'),
(10, '2015-05', 'Medidas Fiscais', '20160601145020_ATIVA Investimentos - Maiov2.pdf', '2016-06-01 17:50:20', '2016-06-01 17:50:20'),
(11, '2015-04', 'A Melhor Previsão do Dólar de Amanhã é o Dólar de Hoje', '20160601145116_ATIVA Investimentos - Abril.pdf', '2016-06-01 17:51:16', '2016-06-01 17:51:16'),
(12, '2015-03', 'Primeiro Trimestre de 2015', '20160601145219_ATIVA Investimentos - Maro - verso01.pdf', '2016-06-01 17:52:19', '2016-06-01 17:52:19'),
(13, '2016-01', 'Crédito Privado em Números', '20160603135356_01 - Janeiro - ATIVA WM.pdf', '2016-06-03 16:53:56', '2016-06-03 16:53:56'),
(14, '2016-02', 'Confie no FGC', '20160603135432_02 - Fevereiro - ATIVA  WM.pdf', '2016-06-03 16:54:32', '2016-06-03 16:54:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `carta_ativa_bg`
--

CREATE TABLE `carta_ativa_bg` (
  `id` int(10) UNSIGNED NOT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `carta_ativa_bg`
--

INSERT INTO `carta_ativa_bg` (`id`, `background`, `created_at`, `updated_at`) VALUES
(1, 'shutterstock_126279959_20160601143538.jpg', '0000-00-00 00:00:00', '2016-06-01 17:35:42');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `saopaulo` text COLLATE utf8_unicode_ci,
  `riodejaneiro` text COLLATE utf8_unicode_ci,
  `curitiba` text COLLATE utf8_unicode_ci,
  `portoalegre` text COLLATE utf8_unicode_ci,
  `belohorizonte` text COLLATE utf8_unicode_ci,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `background`, `saopaulo`, `riodejaneiro`, `curitiba`, `portoalegre`, `belohorizonte`, `facebook`, `twitter`, `email`, `created_at`, `updated_at`) VALUES
(1, 'shutterstock_160901759_20160601151105.jpg', '<p>Rua Iguatemi, 192<br />\r\n15&ordm; e 16&ordm; andares<br />\r\nItaim Bibi<br />\r\nS&atilde;o Paulo/SP<br />\r\n01451-011</p>\r\n\r\n<p>11 4950-7206<br />\r\n11 4097-7206</p>\r\n\r\n<p>Ouvidoria:<br />\r\n0800 282 9900</p>\r\n\r\n<p><a href="mailto:ativawm@ativawm.com.br">ativawm@ativawm.com.br</a></p>\r\n', '<p>Av. das Am&eacute;ricas, 3500<br />\r\nSls 314 318 - Le Monde<br />\r\nEd. Londres<br />\r\nBarra da Tijuca<br />\r\nRio de Janeiro/RJ<br />\r\n22640-102</p>\r\n\r\n<p>21 3515-0200<br />\r\n21 3958-0200</p>\r\n', '<p>Al. Dr. Carlos de Carvalho, 417<br />\r\n30&ordm; andar, Sala 300<br />\r\nCentro<br />\r\nCuritiba/PR<br />\r\n80410-180</p>\r\n\r\n<p>41 3075-7400</p>\r\n', '<p>Av. Nilo Pe&ccedil;anha, 2825<br />\r\nEd. Iguatemi Corporate<br />\r\nConjunto 1503<br />\r\nTr&ecirc;s Figueiras<br />\r\nPorto Alegre/RS<br />\r\n91330-001</p>\r\n\r\n<p>51 3017-8707</p>\r\n', '<p>Rua dos Amor&eacute;s, 2001<br />\r\nSls 405 / 406<br />\r\nLourdes<br />\r\nBelo Horizonte/MG<br />\r\n30140-072</p>\r\n\r\n<p>31 3025-0601</p>\r\n', 'https://www.facebook.com/ativawm', 'https://twitter.com/ativawm', 'ativawm@ativawm.com.br', '0000-00-00 00:00:00', '2016-06-03 16:58:06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mensagem` text COLLATE utf8_unicode_ci,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `documentos`
--

CREATE TABLE `documentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `documentos_categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `documentos`
--

INSERT INTO `documentos` (`id`, `documentos_categoria_id`, `ordem`, `titulo`, `arquivo`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'Código de Conduta e Ética e Anticorrupção 072015', 'codigo-conduta-etica-anticorrupcao_20160531171152.pdf', '2016-05-31 17:11:52', '2016-05-31 17:11:52'),
(2, 1, 1, 'Política de Gerenciamento de Risco 2015', 'politica-de-gerenciamento-de-risco-2015_20160531171224.pdf', '2016-05-31 17:12:24', '2016-05-31 17:12:24'),
(3, 1, 2, 'ANEXO II Fomulário de referência', 'anexoii-fomulario-de-referencia_20160531171239.pdf', '2016-05-31 17:12:39', '2016-05-31 17:12:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `documentos_categorias`
--

CREATE TABLE `documentos_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `documentos_categorias`
--

INSERT INTO `documentos_categorias` (`id`, `ordem`, `titulo`, `created_at`, `updated_at`) VALUES
(1, 0, 'MANUAIS E POLÍTICAS', '2016-05-31 17:10:44', '2016-05-31 17:10:44');

-- --------------------------------------------------------

--
-- Estrutura da tabela `filosofia`
--

CREATE TABLE `filosofia` (
  `id` int(10) UNSIGNED NOT NULL,
  `background_abertura` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto_abertura_1` text COLLATE utf8_unicode_ci,
  `texto_abertura_2` text COLLATE utf8_unicode_ci,
  `background_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_1` text COLLATE utf8_unicode_ci,
  `texto_1` text COLLATE utf8_unicode_ci,
  `background_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_2` text COLLATE utf8_unicode_ci,
  `texto_2` text COLLATE utf8_unicode_ci,
  `background_3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_3` text COLLATE utf8_unicode_ci,
  `texto_3` text COLLATE utf8_unicode_ci,
  `background_4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_4` text COLLATE utf8_unicode_ci,
  `texto_4` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `filosofia`
--

INSERT INTO `filosofia` (`id`, `background_abertura`, `texto_abertura_1`, `texto_abertura_2`, `background_1`, `titulo_1`, `texto_1`, `background_2`, `titulo_2`, `texto_2`, `background_3`, `titulo_3`, `texto_3`, `background_4`, `titulo_4`, `texto_4`, `created_at`, `updated_at`) VALUES
(1, 'shutterstock_142403377_20160601142007.jpg', '<p>Contribuir para a rentabiliza&ccedil;&atilde;o do patrim&ocirc;nio de nossos clientes, por meio de orienta&ccedil;&otilde;es t&eacute;cnicas adequadas e preceitos &eacute;ticos, garantindo relacionamentos fi&eacute;is e duradouros.</p>\r\n', '<p>Nossas parcerias v&atilde;o mais longe porque vemos de perto as necessidades dos clientes.</p>\r\n', 'shutterstock_142403377_20160601142302.jpg', '<p>Solu&ccedil;&otilde;es Financeiras</p>\r\n\r\n<p><strong>Personalizadas</strong></p>\r\n', '<p>Somos uma empresa de investimentos independente e, por isso, temos isen&ccedil;&atilde;o na escolha dos produtos que oferecemos, com transpar&ecirc;ncia, a nossos clientes.</p>\r\n\r\n<p>Nossa longa experi&ecirc;ncia no mercado financeiro &eacute; suportado por uma equipe t&eacute;cnica altamente qualificada e rigorosos padr&otilde;es de governan&ccedil;a e compliance.</p>\r\n\r\n<p>Com a integra&ccedil;&atilde;o desses fatores podemos analisar a situa&ccedil;&atilde;o financeira de cada cliente, recomendar o que realmente acreditamos ser a solu&ccedil;&atilde;o mais adequada e acompanhar com precis&atilde;o a evolu&ccedil;&atilde;o de seu patrim&ocirc;nio.</p>\r\n', 'shutterstock_142403377_20160601142306.jpg', '<p>Investimento de Capital</p>\r\n\r\n<p>com <strong>Seguran&ccedil;a</strong></p>\r\n', '<p>Somos conservadores em rela&ccedil;&atilde;o ao investimento de capital dos nossos clientes.</p>\r\n\r\n<p>Acreditamos que um patrim&ocirc;nio com seriedade e dedica&ccedil;&atilde;o deve ser gerido do mesmo modo e por isso, trabahamos com &eacute;tica, dilig&ecirc;ncia e transpar&ecirc;ncia.<br />\r\nSomos comprometidos em rentabilizar os investimentos dos nossos clientes de forma consciente e fundamentada.</p>\r\n', 'shutterstock_142403377_20160601142310.jpg', '<p>Reputa&ccedil;&atilde;o e <strong>Credibilidade</strong></p>\r\n', '<p>Em mais de trinta anos de atua&ccedil;&atilde;o reunimos em nossa bagagem muita experi&ecirc;ncia, conhecimento t&eacute;cnico e, principalmente, o aprendizado adquirido com os constantes movimentos do mercado financeiro. Somos vitoriosos porque nunca abandonamos nossos ideais e assim seguimos independentes.</p>\r\n\r\n<p>Nossa credibilidade se mostra em relacionamentos longos, baseados em confian&ccedil;a, que nos endossam e geram novas e pr&oacute;speras rela&ccedil;&otilde;es.</p>\r\n', 'shutterstock_142403377_20160601142313.jpg', '<p>Atendimento Qualificado</p>\r\n\r\n<p>e <strong>Comprometido</strong></p>\r\n', '<p>Investimos em treinamento e capacita&ccedil;&atilde;o, pois acreditamos que a excel&ecirc;ncia no atendimento &eacute; a pe&ccedil;a chave para constru&ccedil;&atilde;o de relacionamentos duradouros.</p>\r\n\r\n<p>Valorizamos e reconhecemos nossos profissionais, pois sabemos que eles s&atilde;o a representa&ccedil;&atilde;o m&aacute;xima de nossos valores..</p>\r\n\r\n<p>&Eacute; assim que constru&iacute;mos o nosso modo de atender, visando sempre a&nbsp;satisfa&ccedil;&atilde;o do cliente e a&nbsp;seguran&ccedil;a do seu patrim&ocirc;nio.</p>\r\n', '0000-00-00 00:00:00', '2016-06-03 16:57:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `home_bg`
--

CREATE TABLE `home_bg` (
  `id` int(10) UNSIGNED NOT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `home_bg`
--

INSERT INTO `home_bg` (`id`, `background`, `created_at`, `updated_at`) VALUES
(1, 'teste2_20160601151803.jpg', '0000-00-00 00:00:00', '2016-06-01 18:18:06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imprensa`
--

CREATE TABLE `imprensa` (
  `id` int(10) UNSIGNED NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto` text COLLATE utf8_unicode_ci,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `imprensa`
--

INSERT INTO `imprensa` (`id`, `data`, `titulo`, `slug`, `texto`, `arquivo`, `video_tipo`, `video_codigo`, `created_at`, `updated_at`) VALUES
(1, '2016-03-04', 'Banco central americano decide manter taxa de juros - Globo News', 'banco-central-americano-decide-manter-taxa-de-juros-globo-news', '<p><a href="http://g1.globo.com/globo-news/jornal-globo-news/videos/t/todos-os-videos/v/banco-central-americano-decide-manter-taxa-de-juros/4570475/" target="_blank">Clique aqui</a> e confira o v&iacute;deo da entrevista com o nosso gestor Augusto Barone&nbsp;concedeu para a Globo News sobre taxa de juros.</p>\r\n', '20160601153702_03-maro- globo news.PNG', 'youtube', NULL, '2016-05-31 17:24:44', '2016-06-01 19:04:42'),
(3, '2016-01-08', 'Juros Futuros Avançam Com Aposta de Investidores no Aperto Monetário - Valor Econômico', 'juros-futuros-avancam-com-aposta-de-investidores-no-aperto-monetario-valor-economico', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:37:42', '2016-06-01 22:41:14'),
(4, '2016-01-14', '5 Investimentos seguros para bater a poupança em 2016 - Exame', '5-investimentos-seguros-para-bater-a-poupanca-em-2016-exame', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:42:49', '2016-06-01 22:42:49'),
(5, '2016-02-19', 'Avanço de microempreendedores individuais acende alerta para inadimplência e falências - Agência Estado', 'avanco-de-microempreendedores-individuais-acende-alerta-para-inadimplencia-e-falencias-agencia-estado', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:43:48', '2016-06-01 22:44:59'),
(6, '2016-01-18', 'Taxa Selic deverá subir 0,5 pontos nesta semana - Jornal do Commercio', 'taxa-selic-devera-subir-0-5-pontos-nesta-semana-jornal-do-commercio', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:44:58', '2016-06-01 22:44:58'),
(7, '2016-02-23', 'Avanço de MEIs acende alerta para falências - R7', 'avanco-de-meis-acende-alerta-para-falencias-r7', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:45:47', '2016-06-01 22:46:06'),
(8, '2016-01-13', 'Real se recupera com trégua da China - Valor Econômico', 'real-se-recupera-com-tregua-da-china-valor-economico', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:46:06', '2016-06-01 22:46:06'),
(10, '2016-03-01', 'Bolsas globais sobem com expectativa de que BCs lancem novos estímulos - Folha de S. Paulo', 'bolsas-globais-sobem-com-expectativa-de-que-bcs-lancem-novos-estimulos-folha-de-s-paulo', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:47:49', '2016-06-01 22:47:49'),
(11, '2016-01-22', 'Títulos como CDBs superam a poupança com folga; veja taxas - Exame', 'titulos-como-cdbs-superam-a-poupanca-com-folga-veja-taxas-exame', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:47:53', '2016-06-01 22:48:03'),
(12, '2016-03-18', 'Cenário político puxa para baixo juros futuros de médio e longo prazos - Valor Econômico', 'cenario-politico-puxa-para-baixo-juros-futuros-de-medio-e-longo-prazos-valor-economico', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:49:06', '2016-06-01 22:49:06'),
(13, '2016-04-11', 'Juros futuros recuam com perspectiva de aprovação do impeachment - Valor Econômico', 'juros-futuros-recuam-com-perspectiva-de-aprovacao-do-impeachment-valor-economico', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:49:29', '2016-06-01 22:49:29'),
(14, '2016-03-18', 'Cenário político puxa para baixo juros futuros de médio e longo prazos - UOL Economia', 'cenario-politico-puxa-para-baixo-juros-futuros-de-medio-e-longo-prazos-uol-economia', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:50:11', '2016-06-01 22:50:11'),
(16, '2016-05-18', 'Juros futuros sobem nesta terça com anúncio da nova equipe econômica - Valor Econômico', 'juros-futuros-sobem-nesta-terca-com-anuncio-da-nova-equipe-economica-valor-economico', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:51:27', '2016-06-01 22:51:27'),
(17, '2016-04-11', 'Dólar cai ao menor valor em quase 8 meses - Valor Econômico', 'dolar-cai-ao-menor-valor-em-quase-8-meses-valor-economico', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:51:37', '2016-06-01 22:51:37'),
(18, '2016-04-25', 'Taxa Selic deverá ficar em 14,25% pela 6ª vez seguida - Jornal do Commercio', 'taxa-selic-devera-ficar-em-14-25-pela-6a-vez-seguida-jornal-do-commercio', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:52:18', '2016-06-01 22:52:50'),
(19, '2016-05-19', 'Dólar fecha acima de R$ 3,56 refletindo atuação do BC e ata do Fed - Valor Econômico', 'dolar-fecha-acima-de-r-3-56-refletindo-atuacao-do-bc-e-ata-do-fed-valor-economico', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:52:43', '2016-06-01 22:52:43'),
(20, '2016-05-19', 'Juros futuros sobem sob reflexo do pessimismo com déficit fiscal - UOL Economia', 'juros-futuros-sobem-sob-reflexo-do-pessimismo-com-deficit-fiscal-uol-economia', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-01 22:53:42', '2016-06-01 22:53:42'),
(22, '2016-06-03', 'Juros futuros recuam após dado de emprego abaixo do esperado nos EUA - Valor Econômico', 'juros-futuros-recuam-apos-dado-de-emprego-abaixo-do-esperado-nos-eua-valor-economico', '<p>Confira a mat&eacute;ria completa na imagem abaixo.</p>\r\n', NULL, 'youtube', NULL, '2016-06-06 18:09:06', '2016-06-06 18:09:06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imprensa_bg`
--

CREATE TABLE `imprensa_bg` (
  `id` int(10) UNSIGNED NOT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `imprensa_bg`
--

INSERT INTO `imprensa_bg` (`id`, `background`, `created_at`, `updated_at`) VALUES
(1, 'shutterstock_154942325_20160601150127.jpg', '0000-00-00 00:00:00', '2016-06-01 18:01:35');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imprensa_imagens`
--

CREATE TABLE `imprensa_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `imprensa_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `imprensa_imagens`
--

INSERT INTO `imprensa_imagens` (`id`, `imprensa_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(3, 1, 0, '03-março--globo-news_20160601153812.PNG', '2016-06-01 18:38:12', '2016-06-01 18:38:12'),
(5, 3, 0, '13---Valor---08.01_20160601193926.jpg', '2016-06-01 22:39:26', '2016-06-01 22:39:26'),
(6, 4, 0, '16---Exame---14.01_20160601194341.jpg', '2016-06-01 22:43:41', '2016-06-01 22:43:41'),
(7, 5, 0, '23--Agência-Estado--19.02_20160601194421.jpg', '2016-06-01 22:44:21', '2016-06-01 22:44:21'),
(8, 6, 0, '18---Jornal-do-Commercio_Impresso---18.01_20160601194511.jpg', '2016-06-01 22:45:11', '2016-06-01 22:45:11'),
(9, 8, 0, '20---Valor-Online---13.01_20160601194632.jpg', '2016-06-01 22:46:32', '2016-06-01 22:46:32'),
(10, 7, 0, '24---Portal-R7---23.02_20160601194637.jpg', '2016-06-01 22:46:37', '2016-06-01 22:46:37'),
(11, 10, 0, '3---Follha-de-SP-01.03_20160601194807.jpg', '2016-06-01 22:48:08', '2016-06-01 22:48:08'),
(12, 11, 0, '22---Exame--22.01_20160601194815.jpg', '2016-06-01 22:48:15', '2016-06-01 22:48:15'),
(13, 12, 0, '18--Valor-Econômico---18.03_20160601194924.jpg', '2016-06-01 22:49:24', '2016-06-01 22:49:24'),
(14, 13, 0, '13--Valor-Online---11.04_20160601194948.jpg', '2016-06-01 22:49:48', '2016-06-01 22:49:48'),
(15, 14, 0, '19--Uol-Economia---18.03_20160601195025.jpg', '2016-06-01 22:50:26', '2016-06-01 22:50:26'),
(16, 16, 0, '11---Valor-Online---18.05_20160601195146.jpg', '2016-06-01 22:51:46', '2016-06-01 22:51:46'),
(17, 17, 0, '14--Valor-Online---11.04_20160601195147.jpg', '2016-06-01 22:51:47', '2016-06-01 22:51:47'),
(18, 19, 0, '12---Valor-Online---19.05_20160601195257.jpg', '2016-06-01 22:52:57', '2016-06-01 22:52:57'),
(19, 18, 0, '17--Jornal-do-Commercio---25.04_20160601195258.jpg', '2016-06-01 22:52:58', '2016-06-01 22:52:58'),
(20, 20, 0, '13---Uol-Economia---19.05_20160601195354.jpg', '2016-06-01 22:53:55', '2016-06-01 22:53:55'),
(21, 22, 0, '6--Valor-Online---03.05_20160606150932.jpg', '2016-06-06 18:09:32', '2016-06-06 18:09:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_02_23_040032_create_home_table', 1),
('2016_02_23_040151_create_somos_ativa_table', 1),
('2016_02_23_040413_create_filosofia_table', 1),
('2016_02_23_040830_create_ativa_wealth_table', 1),
('2016_02_23_040928_create_contato_table', 1),
('2016_02_23_144048_create_contatos_recebidos', 1),
('2016_02_25_124703_create_imprensa_table', 1),
('2016_02_25_124713_create_imprensa_imagens_table', 1),
('2016_03_31_175755_create_politicas_table', 1),
('2016_03_31_183109_create_documentos_tables', 1),
('2016_05_30_030250_create_carta_ativa_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `politicas`
--

CREATE TABLE `politicas` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `politicas`
--

INSERT INTO `politicas` (`id`, `texto`, `background`, `created_at`, `updated_at`) VALUES
(1, '<h2 class="politica-titulo">POL&Iacute;TICAS DO SITE E DE NEGOCIA&Ccedil;&Atilde;O</h2>\r\n\r\n<p>As informa&ccedil;&otilde;es contidas neste site s&atilde;o de car&aacute;ter meramente informativo, bem como n&atilde;o se trata de qualquer tipo de aconselhamento para a realiza&ccedil;&atilde;o de investimento, n&atilde;o devendo ser utilizadas com este prop&oacute;sito, nem entendidas como tal, inclusive em qualquer localidade ou jurisdi&ccedil;&atilde;o em que tal oferta, solicita&ccedil;&atilde;o ou venda possa ser contra lei. ATIVA WEALTH MANAGEMENT n&atilde;o comercializa, tampouco distribui cotas de fundos de investimentos ou qualquer outro ativo financeiro.</p>\r\n\r\n<p>Fundos de Investimento n&atilde;o contam com a garantia do administrador, do gestor da carteira, de qualquer mecanismo de seguro, ou, ainda do Fundo Garantidor de Cr&eacute;dito - FGC<br />\r\nAntes de tomar a decis&atilde;o de aplicar em qualquer opera&ccedil;&atilde;o, os potenciais investidores devem considerar cuidadosamente, tendo em vista suas pr&oacute;prias situa&ccedil;&otilde;es financeiras, seus objetivos de investimento, todas as informa&ccedil;&otilde;es dispon&iacute;veis e, em particular, avaliar os fatores de risco aos quais o investimento est&aacute; sujeito.</p>\r\n\r\n<p>As decis&otilde;es de investimento s&atilde;o de responsabilidade total e irrestrita do leitor. A ATIVA WEALTH MANAGEMENT n&atilde;o pode ser responsabilizada por preju&iacute;zos oriundos de decis&otilde;es tomadas com base nas informa&ccedil;&otilde;es aqui apresentadas.</p>\r\n\r\n<p>Para mais informa&ccedil;&otilde;es sobre os produtos relacionadas ao objetivo, riscos, rentabilidade dos &uacute;ltimos anos e outros, acesse o site www.ativainvestimentos.com.br ou entre em contato com a nossa Central de Atendimento: 0800 285 0147.</p>\r\n\r\n<p><strong>Ouvidoria: ATIVA 0800 282 9900.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2 class="politica-titulo">POL&Iacute;TICA DE PRIVACIDADE E SEGURAN&Ccedil;A</h2>\r\n\r\n<p>A Ativa Corretora se compromete com a preserva&ccedil;&atilde;o m&aacute;xima da privacidade de seus clientes. Para manter esse compromisso, adotamos como Pol&iacute;tica de Privacidade n&atilde;o revelar ou divulgar quaisquer informa&ccedil;&otilde;es a respeito de seus usu&aacute;rios, sem a expressa autoriza&ccedil;&atilde;o dos mesmos, a n&atilde;o ser para:</p>\r\n\r\n<p>Cumprir exig&ecirc;ncia legal ou determina&ccedil;&atilde;o de autoridade competente;</p>\r\n\r\n<p>Fornecer dados estat&iacute;sticos, em car&aacute;ter gen&eacute;rico, sobre o acesso e a utiliza&ccedil;&atilde;o das informa&ccedil;&otilde;es disponibilizadas neste site.</p>\r\n\r\n<p>A Ativa poder&aacute; registrar e armazenar em meios magn&eacute;ticos pr&oacute;prios os atos que seus clientes praticarem atrav&eacute;s deste site. Tais dados poder&atilde;o ser utilizados como prova, sempre que necess&aacute;rio.</p>\r\n\r\n<p>Nosso modelo de seguran&ccedil;a garante a integridade dos bancos de dados e de todas as opera&ccedil;&otilde;es feitas pela ATIVA. Conhe&ccedil;a os principais itens:</p>\r\n\r\n<p><strong>Assinatura Eletr&ocirc;nica</strong><br />\r\n&Eacute; uma senha alfanum&eacute;rica, cadastrada pelo cliente em seus primeiros acessos ao site da ATIVA. A Assinatura Eletr&ocirc;nica assegura que s&oacute; voc&ecirc; confirma o envio de ordens e outras opera&ccedil;&otilde;es feitas online.</p>\r\n\r\n<p><strong>Conex&atilde;o segura SSL de 128 bits</strong><br />\r\nEm uma conex&atilde;o segura com um servidor na Internet, os dados transmitidos s&atilde;o criptografados. Desta forma, mesmo que algu&eacute;m consiga interceptar as mensagens, n&atilde;o ser&aacute; capaz de interpret&aacute;-las. Na ATIVA, utilizamos uma conex&atilde;o criptografada de 128 bits, a mais avan&ccedil;ada dispon&iacute;vel para o mercado corporativo. Nossos servidores tamb&eacute;m s&atilde;o certificados por uma ag&ecirc;ncia de auditoria independente, uma confirma&ccedil;&atilde;o da validade das conex&otilde;es codificadas.</p>\r\n\r\n<p><strong>Tempo de inatividade do sistema</strong><br />\r\nPara evitar acessos indesejados &agrave; sua tela de negocia&ccedil;&atilde;o, a ATIVA usa um mecanismo de interrup&ccedil;&atilde;o da conex&atilde;o. Sempre que voc&ecirc; passar mais de uma hora sem navegar em nosso site, a sess&atilde;o ser&aacute; encerrada. Fa&ccedil;a o login novamente quando quiser voltar a usar a p&aacute;gina.</p>\r\n\r\n<p><strong>Firewall</strong><br />\r\nA ATIVA segue o exemplo das maiores institui&ccedil;&otilde;es do mercado e impede qualquer acesso indevido &agrave;s informa&ccedil;&otilde;es dos servidores de aplica&ccedil;&atilde;o e bancos de dados.</p>\r\n\r\n<p><strong>Seguran&ccedil;a da senha do cliente</strong><br />\r\nAssim como sua Assinatura Eletr&ocirc;nica, sua senha &eacute; de uso exclusivo pessoal e intransfer&iacute;vel. A seguran&ccedil;a de suas informa&ccedil;&otilde;es depende do sigilo destes dados, por isso, nunca repasse sua Senha ou Assinatura Eletr&ocirc;nica a terceiros.</p>\r\n\r\n<p>Em caso de suspeita de viola&ccedil;&atilde;o de Senha ou Assinatura Eletr&ocirc;nica, entre em contato com o Atendimento pelo telefone: 4007 2447 para Capitais e Regi&otilde;es Metropolitanas e 0800 285 0147 para Demais Regi&otilde;es, para a altera&ccedil;&atilde;o de seus dados de acesso.</p>\r\n', 'shutterstock_242311573_20160601150849.jpg', '0000-00-00 00:00:00', '2016-06-01 18:08:54');

-- --------------------------------------------------------

--
-- Estrutura da tabela `somos_ativa`
--

CREATE TABLE `somos_ativa` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `somos_ativa`
--

INSERT INTO `somos_ativa` (`id`, `texto`, `video_tipo`, `video_codigo`, `background`, `created_at`, `updated_at`) VALUES
(1, '<p>Os mais de 30 anos de tradi&ccedil;&atilde;o e excel&ecirc;ncia reconhecida no mercado financeiro deixam claro: ATIVA &eacute; uma Corretora que sempre soube ver al&eacute;m.&nbsp;Isso significa olhar atrav&eacute;s de n&uacute;meros e valores, de investidores e seus patrim&ocirc;nios, e poder enxergar sonhos e horizontes, pessoas e suas hist&oacute;rias. A ATIVA v&ecirc; todas as possibilidades&nbsp;dos investimentos e identifica o mais seguro para voc&ecirc;.</p>\r\n\r\n<p>Essa vis&atilde;o ampla foi a ideia central para a constru&ccedil;&atilde;o da ATIVA Investimentos&nbsp;e de sua identidade.&nbsp;Com a capacidade de ver o todo em um mercado de tantos altos e baixos - onde os altos s&atilde;o oportunidades para todos; e os baixos, oportunidades para quem sabe. A&nbsp;ATIVA desenvolveu uma personalidade perspicaz e inteligente, que combina perspectiva &agrave;&nbsp;altura de sua experi&ecirc;ncia e revela cenas que traduzem a tranquilidade da seguran&ccedil;a financeira.</p>\r\n', 'youtube', '4NUrqpKzvSI', 'shutterstock_127845221_20160601142826.jpg', '0000-00-00 00:00:00', '2016-06-03 16:57:17');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$IkxoPtJ/hCbSNPaEOQHJIOFucNonJT4IOZd7eB8cH0D5uMVRhRipS', 'IERrZda2oOMWRpLW0nd9a9OIlxzWZWLPIDjJc2wyJeLmXhsrwgBOdpR1BDB6', '0000-00-00 00:00:00', '2016-05-31 23:17:52'),
(2, 'ativa', 'contato@ativawm.com.br', '$2y$10$pLT2VliTKvxqFF0ZBSiN6e52knVWUDc/PWcxFE/j5jZyMDGED9Kae', 'LEfyzivjBz46t9AYRfKpusfPZHAWQuG3fgtWQt6ERAbX9i756CotpBEs5tCK', '2016-05-31 23:18:25', '2016-06-03 19:26:20'),
(3, 'Marketing', 'comunicacao@ativainvestimentos.com.br', '$2y$10$BAsQXjnWwwIkyebQDo01L.cAVfhGIee.lBPGy3XjUCAKKw5B1wkeS', 'ExGn1zZeyemWWtCybSNu5fpcR7wlZvsUe8N8Wac3Gt6GWy2CtaZkeQfLfdgR', '2016-06-01 21:42:00', '2016-06-01 22:34:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ativa_wealth`
--
ALTER TABLE `ativa_wealth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carta_ativa`
--
ALTER TABLE `carta_ativa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carta_ativa_bg`
--
ALTER TABLE `carta_ativa_bg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documentos`
--
ALTER TABLE `documentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documentos_documentos_categoria_id_foreign` (`documentos_categoria_id`);

--
-- Indexes for table `documentos_categorias`
--
ALTER TABLE `documentos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filosofia`
--
ALTER TABLE `filosofia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_bg`
--
ALTER TABLE `home_bg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imprensa`
--
ALTER TABLE `imprensa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imprensa_bg`
--
ALTER TABLE `imprensa_bg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imprensa_imagens`
--
ALTER TABLE `imprensa_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `imprensa_imagens_imprensa_id_foreign` (`imprensa_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `politicas`
--
ALTER TABLE `politicas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `somos_ativa`
--
ALTER TABLE `somos_ativa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ativa_wealth`
--
ALTER TABLE `ativa_wealth`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `carta_ativa`
--
ALTER TABLE `carta_ativa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `carta_ativa_bg`
--
ALTER TABLE `carta_ativa_bg`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `documentos`
--
ALTER TABLE `documentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `documentos_categorias`
--
ALTER TABLE `documentos_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `filosofia`
--
ALTER TABLE `filosofia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `home_bg`
--
ALTER TABLE `home_bg`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `imprensa`
--
ALTER TABLE `imprensa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `imprensa_bg`
--
ALTER TABLE `imprensa_bg`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `imprensa_imagens`
--
ALTER TABLE `imprensa_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `politicas`
--
ALTER TABLE `politicas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `somos_ativa`
--
ALTER TABLE `somos_ativa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `documentos`
--
ALTER TABLE `documentos`
  ADD CONSTRAINT `documentos_documentos_categoria_id_foreign` FOREIGN KEY (`documentos_categoria_id`) REFERENCES `documentos_categorias` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `imprensa_imagens`
--
ALTER TABLE `imprensa_imagens`
  ADD CONSTRAINT `imprensa_imagens_imprensa_id_foreign` FOREIGN KEY (`imprensa_id`) REFERENCES `imprensa` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
